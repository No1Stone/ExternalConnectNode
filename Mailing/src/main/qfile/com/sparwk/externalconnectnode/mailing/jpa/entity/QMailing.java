package com.sparwk.externalconnectnode.mailing.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMailing is a Querydsl query type for Mailing
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMailing extends EntityPathBase<Mailing> {

    private static final long serialVersionUID = 1372065538L;

    public static final QMailing mailing = new QMailing("mailing");

    public final StringPath content = createString("content");

    public final StringPath fromEmail = createString("fromEmail");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath sendYn = createString("sendYn");

    public final StringPath status = createString("status");

    public final StringPath subject = createString("subject");

    public final StringPath toEmail = createString("toEmail");

    public QMailing(String variable) {
        super(Mailing.class, forVariable(variable));
    }

    public QMailing(Path<? extends Mailing> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMailing(PathMetadata metadata) {
        super(Mailing.class, metadata);
    }

}

