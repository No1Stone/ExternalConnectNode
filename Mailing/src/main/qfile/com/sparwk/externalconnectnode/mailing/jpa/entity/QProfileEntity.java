package com.sparwk.externalconnectnode.mailing.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileEntity is a Querydsl query type for ProfileEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileEntity extends EntityPathBase<ProfileEntity> {

    private static final long serialVersionUID = 393862627L;

    public static final QProfileEntity profileEntity = new QProfileEntity("profileEntity");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath profileId = createString("profileId");

    public QProfileEntity(String variable) {
        super(ProfileEntity.class, forVariable(variable));
    }

    public QProfileEntity(Path<? extends ProfileEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileEntity(PathMetadata metadata) {
        super(ProfileEntity.class, metadata);
    }

}

