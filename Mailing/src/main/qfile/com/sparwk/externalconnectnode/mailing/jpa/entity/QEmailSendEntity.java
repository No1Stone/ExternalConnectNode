package com.sparwk.externalconnectnode.mailing.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QEmailSendEntity is a Querydsl query type for EmailSendEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEmailSendEntity extends EntityPathBase<EmailSendEntity> {

    private static final long serialVersionUID = 206074750L;

    public static final QEmailSendEntity emailSendEntity = new QEmailSendEntity("emailSendEntity");

    public final NumberPath<Long> accountid = createNumber("accountid", Long.class);

    public final StringPath customerMessage = createString("customerMessage");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath receiver = createString("receiver");

    public final DateTimePath<java.time.LocalDateTime> sendDateTimeUser = createDateTime("sendDateTimeUser", java.time.LocalDateTime.class);

    public final DateTimePath<java.time.LocalDateTime> sendDateTimeUTC0 = createDateTime("sendDateTimeUTC0", java.time.LocalDateTime.class);

    public final StringPath sender = createString("sender");

    public final StringPath useForm = createString("useForm");

    public final StringPath userIp = createString("userIp");

    public QEmailSendEntity(String variable) {
        super(EmailSendEntity.class, forVariable(variable));
    }

    public QEmailSendEntity(Path<? extends EmailSendEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailSendEntity(PathMetadata metadata) {
        super(EmailSendEntity.class, metadata);
    }

}

