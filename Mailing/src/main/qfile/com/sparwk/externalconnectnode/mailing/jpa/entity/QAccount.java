package com.sparwk.externalconnectnode.mailing.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccount is a Querydsl query type for Account
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccount extends EntityPathBase<Account> {

    private static final long serialVersionUID = -636225948L;

    public static final QAccount account = new QAccount("account");

    public final StringPath accntEmail = createString("accntEmail");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath accntLoginActiveYn = createString("accntLoginActiveYn");

    public final StringPath accntPass = createString("accntPass");

    public final NumberPath<Long> accntRepositoryId = createNumber("accntRepositoryId", Long.class);

    public final StringPath accntTypeCd = createString("accntTypeCd");

    public final StringPath countryCd = createString("countryCd");

    public final NumberPath<Long> lastUseProfileId = createNumber("lastUseProfileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath personalInfoCollectionYn = createString("personalInfoCollectionYn");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final StringPath provideInfoTosparwkYn = createString("provideInfoTosparwkYn");

    public final StringPath provideInfoTothirdYn = createString("provideInfoTothirdYn");

    public final StringPath receiveMarketingInfoYn = createString("receiveMarketingInfoYn");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath transferInfoToabroadYn = createString("transferInfoToabroadYn");

    public final StringPath useYn = createString("useYn");

    public final StringPath verifyPhoneYn = createString("verifyPhoneYn");

    public final StringPath verifyYn = createString("verifyYn");

    public QAccount(String variable) {
        super(Account.class, forVariable(variable));
    }

    public QAccount(Path<? extends Account> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccount(PathMetadata metadata) {
        super(Account.class, metadata);
    }

}

