package com.sparwk.externalconnectnode.mailing.biz.v1.bach;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.sparwk.externalconnectnode.mailing.biz.v1.enumdata.AwsSesErrorStatusEnum;
import com.sparwk.externalconnectnode.mailing.biz.v1.enumdata.YnEnum;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.AwsSesService;
import com.sparwk.externalconnectnode.mailing.jpa.entity.Mailing;
import com.sparwk.externalconnectnode.mailing.jpa.repository.MailingRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@EnableScheduling
@RequiredArgsConstructor
public class MailingBach implements Serializable {

    private final Logger logger = LoggerFactory.getLogger(MailingBach.class);
    private final MailingRepository mailingRepository;
    private final AwsSesService awsSesService;
    @Value("${cloud.aws.devmail}")
    private String devemail;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void EmailSendBachSchedl() throws InterruptedException {
        logger.info("EmailSendBachSchedl");
        int sec = 0;

        if (mailingRepository.existsBySendYn(YnEnum.N.getValue())) {
            List<Mailing> sendItem = new ArrayList<>();
            sendItem = mailingRepository
                    .findBySendYnAndStatus(YnEnum.N.getValue(), String.valueOf(AwsSesErrorStatusEnum.INIT.getStatus()));
            for (int i = 0; i < sendItem.size(); i++) {

                if (sendItem.get(i).getStatus().equals("100")) {
                    String[] rece = {sendItem.get(i).getToEmail()};
                    awsSesService.TextSendMail(rece, devemail, sendItem.get(i).getContent(), sendItem.get(i).getSubject());

                    mailingRepository.save(Mailing
                            .builder()
                            .toEmail(sendItem.get(i).getToEmail())
                            .fromEmail(sendItem.get(i).getFromEmail())
                            .subject(sendItem.get(i).getSubject())
                            .content(sendItem.get(i).getContent())
                            .sendYn(YnEnum.Y.getValue())
                            .status(String.valueOf(AwsSesErrorStatusEnum.Successful_delivery.getStatus()))
                            .regDt(sendItem.get(i).getRegDt())
                            .regUsr(sendItem.get(i).getRegUsr())
                            .modUsr(sendItem.get(i).getModUsr())
                            .modDt(sendItem.get(i).getModDt())
                            .build());
                    mailingRepository.flush();
                }

                try {
                } catch (AmazonServiceException e) {
                    mailingRepository.save(Mailing
                            .builder()
                            .toEmail(sendItem.get(i).getToEmail())
                            .fromEmail(sendItem.get(i).getFromEmail())
                            .subject(sendItem.get(i).getSubject())
                            .content(sendItem.get(i).getContent())
                            .sendYn(YnEnum.N.getValue())
                            .status(String.valueOf(e.getStatusCode()))
                            .regDt(sendItem.get(i).getRegDt())
                            .regUsr(sendItem.get(i).getRegUsr())
                            .modUsr(sendItem.get(i).getModUsr())
                            .modDt(sendItem.get(i).getModDt())
                            .build());
                }
                if (sec > 9) {
                    logger.info("sec = {}", sec);
                    //인써트 하고 내용저장하는시간이 30개 기준 3초걸림
                    //무슨짓을해도 익셉션안날듯
                    Thread.sleep(500);
                    sec = 0;
                }
                sec++;
            }
            mailingRepository.flush();
            sendItem.clear();
        }
    }

    @Scheduled(cron = "0/59 0/10 * * * ?")
    @Transactional
    public void EmailDeleteBachSchedl() throws InterruptedException {
        logger.info("EmailDeleteBachSchedl");
        mailingRepository.deleteAll(mailingRepository
                .findBySendYnAndStatus(YnEnum.Y.getValue(),
                        String.valueOf(AwsSesErrorStatusEnum.Successful_delivery.getStatus())));
        mailingRepository.flush();
    }

    @Scheduled(cron = "0/59 0/10 * * * ?")
    @Transactional
    public void EmailReSendBachSchedl() throws InterruptedException {
        logger.info("EmailDeleteBachSchedl");
        mailingRepository.saveAllAndFlush(mailingRepository
                .findBySendYnAndStatus(YnEnum.N.getValue(),
                        String.valueOf(AwsSesErrorStatusEnum.Maximum_send_rate_exceeded.getStatus()))
                .stream().peek(e -> {
                    e.setStatus(String.valueOf(AwsSesErrorStatusEnum.INIT.getStatus()));
                    e.setSendYn(YnEnum.N.getValue());
                })
                .collect(Collectors.toList())
        );
        mailingRepository.flush();
    }
}
