package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class AdminPasswordCreateRequest {

    @Schema(description = "비밀번호")
    private String password;
    @Schema(description = "비밀번호")
    private String key;
    @Schema(description = "제목")
    private Long accountId;

}
