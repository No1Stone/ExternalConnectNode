package com.sparwk.externalconnectnode.mailing.jpa.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class MediaVerifyBaseDTO {

    private Long accntId;
    private String mediaVerifyReqId;
    private String mediaTypeCd;
    private String mediaValidString;
    private String verifyTypeCd;
    private LocalDateTime expiredDt;
    private LocalDateTime regDt;

}
