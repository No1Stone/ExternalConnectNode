package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class EmailSendRequest {

    @Schema(description = "사용할 메일 템플릿 비밀번호 리셋 ResetPassword, 이메일 인증 UserCertification")
    private String useForm;
    @Schema(description = "받는사람 배열! \"receiver\" : [\"jangws1003.ui@gmail.com\"], ")
    private String[] receiver;
    @Schema(description = " 메세지를 보내는 템플릿인 경우 사용되는 메세지")
    private String customerMessage;



}
