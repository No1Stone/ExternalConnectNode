package com.sparwk.externalconnectnode.mailing.biz.v1.service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AwsSesService {
	private final Logger logger = LoggerFactory.getLogger(AwsSesService.class);
	@Autowired
	private AmazonSimpleEmailService ases;

	public void AuthEmail(String TO, String FROM, String TEXTBODY, String SUBJECT) {
		SendEmailRequest ser = new SendEmailRequest().withDestination(new Destination().withToAddresses(TO))//받는사람
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(TEXTBODY)))
						.withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
				.withSource(FROM);//보내는놈
		ases.sendEmail(ser);

	}
	public void TempleateSendEmail(String[] to, String from, String temBody, String subject) {
		SendEmailRequest ser = new SendEmailRequest().withDestination(new Destination().withToAddresses(to))//받는사람
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(temBody)))
						.withSubject(new Content().withCharset("UTF-8").withData(subject)))
				.withSource(from);//보내는놈
		ases.sendEmail(ser);
	}
	public void TextSendMail(String[] TO, String FROM, String TEXT, String SUBJECT) {
		SendEmailRequest ser = new SendEmailRequest().withDestination(new Destination().withToAddresses(TO))//받는사람
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(TEXT)))
						.withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
				.withSource(FROM);//보내는놈
		ases.sendEmail(ser);
	}
	public void ArrayfromSendMail(String[] emailTO, String FROM, String TEXT, String SUBJECT) {
		//emailTO 배열형태도되고 emailTo[0],emailTo[1] 이거도됨
		SendEmailRequest ser = new SendEmailRequest().withDestination(new Destination().withToAddresses(emailTO))//받는사람
				.withMessage(new Message()
						.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(TEXT)))
						.withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
				.withSource(FROM);//보내는놈
		ases.sendEmail(ser);
	}

	/*
	 * 
	 * // Replace recipient@example.com with a "To" address. If your account // is
	 * still in the sandbox, this address must be verified. static final String TO =
	 * "jangws1003.ui@gmail.com"; //static final String TO = "pjh00032@gmail.com";
	 * //static final String TO = "choiminwook.ui@gmail.com";
	 * 
	 * 
	 * 
	 * // The configuration set to use for this email. If you do not want to use a
	 * // configuration set, comment the following variable and the //
	 * .withConfigurationSetName(CONFIGSET); argument below. //static final String
	 * CONFIGSET = "ConfigSet";
	 * 
	 * // The subject line for the email. static final String SUBJECT = "이게제목이고";
	 * static final String FROM = "jangws1003.ui@gmail.com"; //static final String
	 * FROM = "NoReply@sparwk.com";
	 * 
	 * // The HTML body for the email. static final String HTMLBODY =
	 * "<h1>이게 본문제목이고</h1>" + "<p>마크다운언어 자리고 <a href='https://aws.amazon.com/ses/'>"
	 * + "Amazon SES</a> 링크가걸리네 <a href='https://aws.amazon.com/sdk-for-java/'>" +
	 * "마크다운은다되나?</a>";
	 * 
	 * // The email body for recipients with non-HTML email clients. static final
	 * String TEXTBODY = "This email was sent through Amazon SES " +
	 * "using the AWS SDK for Java.";
	 */

//	지워도됨
//	public void test1Method() {
//		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
//		try {
//		      AmazonSimpleEmailService client = 
//		          AmazonSimpleEmailServiceClientBuilder.standard()
//		          // Replace US_WEST_2 with the AWS Region you're using for
//		          // Amazon SES.
//		          .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//		            .withRegion(Regions.AP_NORTHEAST_2).build();
//		    
//		      SendEmailRequest request = new SendEmailRequest()
//		          .withDestination(
//		              new Destination().withToAddresses(TO))
//		          .withMessage(new Message()
//		              .withBody(new Body()
//		                  .withHtml(new Content()
//		                  //    .withCharset("UTF-8").withData(HTMLBODY))
//		                  //.withText(new Content()
//		                      .withCharset("UTF-8").withData(TEXTBODY)))
//		              .withSubject(new Content()
//		                  .withCharset("UTF-8").withData(SUBJECT)))
//		          .withSource(FROM)
//		          //configset 도큐먼트찾아보기 
//		        //  .withConfigurationSetName(CONFIGSET)
//		          ;
//	
//		      client.sendEmail(request);
//		      System.out.println("Email sent!");
//		    } catch (Exception ex) {
//		      System.out.println("The email was not sent. Error message: " 
//		          + ex.getMessage());
//		    }
//	}

}
