package com.sparwk.externalconnectnode.mailing.biz.v1;

import com.sparwk.externalconnectnode.mailing.biz.v1.dto.EmailSendRequest;
import com.sparwk.externalconnectnode.mailing.biz.v1.dto.MobileVerifyRequest;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.SESServiceRepository;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.SNSService;
import com.sparwk.externalconnectnode.mailing.config.filter.responsepack.Response;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/V1/verify")
@CrossOrigin("*")
public class VerifyController {

    @Autowired
    private SNSService snsService;
    @Autowired
    private SESServiceRepository sesServiceRepository ;
    private final Logger logger = LoggerFactory.getLogger(VerifyController.class);

    @PostMapping(path = "/mobile")
    public Response MobileVerifySendController(@RequestBody MobileVerifyRequest mobileDto, HttpServletRequest req){
        logger.info(" - {}",mobileDto);
        Response res = snsService.MobileVerifySendService(mobileDto.getPhoneNumber(), req);
        return res;
    }

    @GetMapping(path = "/mobile/{number}")
    public Response MobileVerifyReciveController(@PathVariable(name = "number")String number, HttpServletRequest req){
        logger.info("numberlog - {}", number);
        Response res = snsService.MobileVerifyReciveService(number, req);
                return res;
    }


    @ApiOperation(
            value = "메일 보내는 에이피아이"
    )
    @Operation(
            description = "템플릿 ResetPassword  /  UserCertification",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "메일보내기")
            }
    )

    @PostMapping(path = "/email")
    public void SendEmailTemplate(@RequestBody EmailSendRequest entity, HttpServletRequest req){
        logger.info("entity - {} ",entity);
        sesServiceRepository.emailTemplateChoiceService(entity);
    }

}
