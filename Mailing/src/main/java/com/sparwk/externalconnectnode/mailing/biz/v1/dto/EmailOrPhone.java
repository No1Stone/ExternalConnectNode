package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class EmailOrPhone {

    private String emailOrPhone;
}
