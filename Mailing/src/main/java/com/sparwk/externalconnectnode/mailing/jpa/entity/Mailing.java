package com.sparwk.externalconnectnode.mailing.jpa.entity;

import com.sparwk.externalconnectnode.mailing.jpa.id.MailingId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tb_mailing")
@IdClass(MailingId.class)
public class Mailing {

    @Id
    @Column(name = "to_email", nullable = true)
    private String toEmail;
    @Id
    @Column(name = "from_email", nullable = true)
    private String fromEmail;
    @Id
    @Column(name = "subject", nullable = true)
    private String subject;
    @Column(name = "content", nullable = true)
    private String content;
    @Column(name = "send_yn", nullable = true)
    private String sendYn;
    @Column(name = "status", nullable = true)
    private String status;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    Mailing(
            String toEmail,
            String fromEmail,
            String subject,
            String content,
            String sendYn,
            String status,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.toEmail = toEmail;
        this.fromEmail = fromEmail;
        this.subject = subject;
        this.content = content;
        this.sendYn = sendYn;
        this.status = status;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
