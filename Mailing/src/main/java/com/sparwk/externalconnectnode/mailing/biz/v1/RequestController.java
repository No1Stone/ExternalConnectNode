package com.sparwk.externalconnectnode.mailing.biz.v1;

import com.sparwk.externalconnectnode.mailing.biz.v1.service.SESServiceRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/V1")
public class RequestController {
    private final Logger logger = LoggerFactory.getLogger(RequestController.class);

    @Value("${sparwk.node.frontupdatepass}")
    private String frontPassURL;

    @Value("${sparwk.node.frontBaseURL}")
    private String frontBaseURL;

    @Autowired
    private SESServiceRepository sesServiceRepository;

    @ApiOperation(
            value = "이메일 인증이 들어오는 에이피아이 메일에서 이메일확인을 누르면 이곳으로 들어옵니다."
    )
    @Operation(
            description = "템플릿 ResetPassword  /  UserCertification",
            parameters = {
                    @Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "메일보내기")
            }
    )
    @RequestMapping(path = "/emailcheck"
            , method = RequestMethod.GET
    )
//    @GetMapping(path = "/emailcheck")
    public String emailcheckinit(@RequestParam(value = "accId") String accId,
                                 @RequestParam(value = "key") String key) {
        logger.info(accId);
        logger.info(key);

        if (sesServiceRepository.ExistsEmailVerifyCheckBooleanService(Long.parseLong(accId), key)) {
            sesServiceRepository.emailVerifyCheckService(Long.parseLong(accId), key);
            StringBuffer sb = new StringBuffer();
            sb.append("redirect:");
            sb.append(frontBaseURL);
            sb.append("/login");
            return sb.toString();
        } else {
            StringBuffer sb = new StringBuffer();
            sb.append("redirect:");
            sb.append(frontBaseURL);
            sb.append("/login");
            return sb.toString();
        }
    }

    @RequestMapping(path = "/test111"
            , method = RequestMethod.GET
    )
    public String test111() {
        StringBuffer sb = new StringBuffer();
        sb.append("redirect:");
        sb.append(frontPassURL);
        sb.append("email=");
        return sb.toString();
    }

    @Value("${sparwk.node.frontadminpass}")
    private String adminPasswordURL;

    @RequestMapping(path = "/adminMail"
            , method = RequestMethod.GET
    )
    public String adminMail(@RequestParam(value = "accId") String accId,
                            @RequestParam(value = "key") String key) {
        logger.info("adminMail init - - -{}", key);
        logger.info("adminMail init - - -{}", accId);
        logger.info(accId);
        logger.info(key);

        StringBuffer sb = new StringBuffer();
        sb.append("redirect:");
        sb.append(adminPasswordURL + "?accId=" + accId + "&key=" + key);
        logger.info(sb.toString());
        return sb.toString();
    }

}
