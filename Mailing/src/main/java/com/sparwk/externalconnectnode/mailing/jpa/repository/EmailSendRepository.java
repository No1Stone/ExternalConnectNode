package com.sparwk.externalconnectnode.mailing.jpa.repository;

import com.sparwk.externalconnectnode.mailing.jpa.entity.EmailSendEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailSendRepository extends JpaRepository<EmailSendEntity, Long>  {
}
