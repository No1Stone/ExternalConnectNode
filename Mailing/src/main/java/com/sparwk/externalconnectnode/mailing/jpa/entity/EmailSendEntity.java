package com.sparwk.externalconnectnode.mailing.jpa.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "email_log")
public class EmailSendEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = true)
    private Long accountid;
    @Column(nullable = true)
    private String useForm;
    @Column(nullable = true)
    private String receiver;
    @Column(nullable = true)
    private String sender;
    @Column(nullable = true)
    private String userIp;
    @Column(nullable = true)
    private LocalDateTime sendDateTimeUTC0;
    @Column(nullable = true)
    private LocalDateTime sendDateTimeUser;
    @Column(nullable = true)
    private String customerMessage;

    @Builder
    EmailSendEntity(
        Long id,
        Long accountid,
        String useForm,
        String receiver,
        String sender,
        String userIp,
        LocalDateTime sendDateTimeUTC0,
        LocalDateTime sendDateTimeUser,
        String customerMessage
        ){
        this.id = id;
        this.accountid = accountid;
        this.useForm = useForm;
        this.receiver = receiver;
        this.sender = sender;
        this.userIp = userIp;
        this.sendDateTimeUTC0 = sendDateTimeUTC0;
        this.sendDateTimeUser = sendDateTimeUser;
        this.customerMessage = customerMessage;
    }

}
