package com.sparwk.externalconnectnode.mailing.jpa.repository;

import com.sparwk.externalconnectnode.mailing.jpa.entity.Mailing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MailingRepository extends JpaRepository<Mailing, String> {

    boolean existsBySendYn(String sendYn);


    List<Mailing> findBySendYnAndStatus(String sendYn, String status);
}
