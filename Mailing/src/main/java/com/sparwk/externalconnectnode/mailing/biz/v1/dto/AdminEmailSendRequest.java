package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class AdminEmailSendRequest {

    @Schema(description = "내용")
    private String contents;
    @Schema(description = "제목")
    private String subject;
    @Schema(description = "받는사람 배열! \"receiver\" : [\"jangws1003.ui@gmail.com\"], ")
    private String[] receiver;

}
