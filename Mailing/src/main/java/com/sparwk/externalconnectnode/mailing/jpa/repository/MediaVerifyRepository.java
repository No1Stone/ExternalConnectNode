package com.sparwk.externalconnectnode.mailing.jpa.repository;

import com.sparwk.externalconnectnode.mailing.jpa.entity.MediaVerify;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MediaVerifyRepository extends JpaRepository<MediaVerify, Long> {

    List<MediaVerify> findByAccntIdAndMediaValidString(Long accId, String key);
    int countByAccntIdAndMediaValidStringAndMediaTypeCd(Long accountId, String mediaValid, String mediaType);
    Optional<MediaVerify> findByAccntIdAndMediaValidStringAndMediaTypeCd(Long accountId, String mediaValid, String mediaType);
    int countByMediaVerifyReqIdAndMediaValidString(String email, String verifyNumber);
    Optional<MediaVerify> findByMediaVerifyReqIdAndMediaValidString(String email, String verifyNumber);

    boolean existsByAccntIdAndMediaValidString(Long accid, String verifyNum);

}
