package com.sparwk.externalconnectnode.mailing.jpa.repository;

import com.sparwk.externalconnectnode.mailing.jpa.entity.Account;
import com.sparwk.externalconnectnode.mailing.jpa.repository.dsl.AccountRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> , AccountRepositoryDynamic {

    Optional<Account> findByAccntEmail(String email);
    int countByAccntEmail(String email);
    Optional<Account> findByPhoneNumberAndVerifyPhoneYn(String phoneNumber, String phoneVerifiYn);
    int countByPhoneNumberAndVerifyPhoneYn(String phoneNumber, String phoneVerifiYn);

}
