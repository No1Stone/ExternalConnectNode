package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RsvpSendForm {

    private Long projId;
    private Long rsvpProfileId;
    private String SenderEmail;
    private String receiverEmail;
    private int memberCount;
    private String projectPassword;
    private String projectTitle;
    private String projectDesctip;
    private String projectSubDesctip;
    private String ownerName;
    private String ownerHeadLine;
    private String ownerProfileImg;
    private String memberName;
}
