package com.sparwk.externalconnectnode.mailing.biz.v1.enumdata;

public enum AwsSesErrorStatusEnum {

    INIT(100, "서버 입력", "NEW INSTANCE"),
    Authentication_successful(235, "인증 성공", "Authentication_successful"),
    Successful_delivery(250,"배송 성공","Successful_delivery"),
    Service_unavailable(421,"서비스 이용 불가","Service_unavailable"),
    Local_processing_error(451,"AWS로컬 처리 오류","Local_processing_error"),
    Timeout(451,"시간 초과","Timeout"),
    Daily_sending_quota_exceeded(454,"일일 전송 할당량 초과","Daily_sending_quota_exceeded"),
    Maximum_send_rate_exceeded(454,"최대 전송 속도 초과","Maximum_send_rate_exceeded"),
    validating_SMTP_credentials(454,"SMTP 자격 증명 검증 시 Amazon SES 문제","validating_SMTP_credentials"),
    Problem_receiving_the_request(454,"요청 수신 문제","Problem_receiving_the_request"),
    Incorrect_credentials(530,"잘못된 자격 증명","Incorrect_credentials"),
    Authentication_Credentials_Invalid(535,"인증 자격 증명이 잘못되었습니다.","Authentication_Credentials_Invalid"),
    Account_not_subscribed_to_Amazon(535,"Amazon SES에 가입하지 않은 계정","Account_not_subscribed_to_Amazon"),
    Message_is_too_long(552,"메시지가 너무 깁니다.","Message_is_too_long"),
    User_not_authorized_to_call_the_Amazon_SES_SMTP_endpoint(554,"Amazon SES SMTP 엔드포인트를 호출할 권한이 없는 사용자","User_not_authorized_to_call_the_Amazon_SES_SMTP_endpoint"),
    Unverified_email_address(554,"확인되지 않은 이메일 주소","Unverified_email_address");

    private int status;
    private String kr;
    private String en;

    AwsSesErrorStatusEnum(int status, String kr, String en){
        this.status = status;
        this.kr = kr;
        this.en = en;
    }

    public int getStatus(){
        return status;
    }

}
