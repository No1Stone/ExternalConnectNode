package com.sparwk.externalconnectnode.mailing.jpa.id;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class MailingId implements Serializable {

    private String toEmail;
    private String fromEmail;
    private String subject;

}
