package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AccountResponse {

    @Schema(description = "계정 아이디")
    private Long accntId;

    @Schema(description = "멀티 계정 그룹핑 아이디")
    private Long accntRepositoryId;

    @Schema(description = "유저 타입 사람/회사/그룹/단체")
    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String accntTypeCd;

    @Schema(description = "사용자 이메일")
    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;

    @Schema(description = "사용자 비밀번호")
    @Size(max = 200, message = "encoding")
    private String accntPass;

    @Schema(description = "사용자 국가 코드")
    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String countryCd;

    @Size(max = 20, message = "")
    @Schema(description = "사용자 핸드폰 번호")
    private String phoneNumber;

    @Size(max = 1, message = "y or n")
    @Schema(description = "접속이메일사용여부 default y")
    private String accntLoginActiveYn;

    @Schema(description = "마지막으로 이용한 프로필 아이디")
    private Long lastUseProfileId;

    @Size(max = 1, message = "y or n")
    @Schema(description = "이메일 인증 여부 default n")
    private String verifyYn;

    @Size(max = 1, message = "y or n")
    @Schema(description = "이메일 인증 여부 default y")
    private String useYn;

    @Size(max = 1, message = "y or n")
    @Schema(description = "핸드폰번호 승인여부")
    private String verifyPhoneYn;

    @Schema(description = "SignUp개인정보 동의1 마케팅 ")
    private String receiveMarketingInfoYn ;

    @Schema(description = "SignUp개인정보 동의2 제 3자 제공")
    private String provideInfoTothirdYn ;

    @Schema(description = "SignUp개인정보 동의3 개인정보 수집동의 I agree to the")
    private String personalInfoCollectionYn ;

    @Schema(description = "SignUp개인정보 동의4 스파크가 내정보를 이용해도되는지 iagree that Sparwk May")
    private String provideInfoTosparwkYn ;

    @Schema(description = "SignUp개인정보 동의5 스파크가 내정보를 해외에 이관해도되는지 iagree that Sparwk May")
    private String transferInfoToabroadYn ;

}
