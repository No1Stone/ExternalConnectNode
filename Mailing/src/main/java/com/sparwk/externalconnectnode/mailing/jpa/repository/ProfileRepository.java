package com.sparwk.externalconnectnode.mailing.jpa.repository;

import com.sparwk.externalconnectnode.mailing.jpa.entity.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<ProfileEntity, Long> {

    Optional<ProfileEntity> findByProfileId(String profileId);


}
