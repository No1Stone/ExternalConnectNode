package com.sparwk.externalconnectnode.mailing.biz.v1.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailPasswordValid {
    private String email;
    private String number;
    private String password;
}
