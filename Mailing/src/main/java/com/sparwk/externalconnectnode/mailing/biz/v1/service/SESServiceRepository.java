package com.sparwk.externalconnectnode.mailing.biz.v1.service;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.externalconnectnode.mailing.biz.CustomException;
import com.sparwk.externalconnectnode.mailing.biz.v1.dto.*;
import com.sparwk.externalconnectnode.mailing.config.common.ClientIp;
import com.sparwk.externalconnectnode.mailing.config.common.DateUtils;
import com.sparwk.externalconnectnode.mailing.config.common.VerifyNumber;
import com.sparwk.externalconnectnode.mailing.jpa.dto.AccountBaseDTO;
import com.sparwk.externalconnectnode.mailing.jpa.dto.EmailverifyDTO;
import com.sparwk.externalconnectnode.mailing.jpa.dto.MediaVerifyBaseDTO;
import com.sparwk.externalconnectnode.mailing.jpa.entity.MediaVerify;
import com.sparwk.externalconnectnode.mailing.jpa.repository.AccountRepository;
import com.sparwk.externalconnectnode.mailing.jpa.repository.EmailSendRepository;
import com.sparwk.externalconnectnode.mailing.jpa.repository.MediaVerifyRepository;
import com.sparwk.externalconnectnode.mailing.jpa.repository.ProfileRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SESServiceRepository {

    private final Logger logger = LoggerFactory.getLogger(SESServiceRepository.class);

    private EntityManager em;
    private JPAQueryFactory queryFactory;

    private SESServiceRepository(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    //test repository
    @Autowired
    private EmailSendRepository emailSendRepository;
    @Autowired
    private ClientIp clientIp;
    @Autowired
    private MailTemplateReadService mailTemplateReadService;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private AwsSesService awsSesService;

    @Autowired
    private MediaVerifyRepository mediaVerifyRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${cloud.aws.devmail}")
    private String devemail;
    @Value("${sparwk.node.frontinvatation}")
    private String invitation;
    @Value("${sparwk.node.frontrejec}")
    private String reject;

    @Value("${sparwk.node.externalconnect.mailingip}")
    private String mailing;


    public boolean ExistsEmailVerifyCheckBooleanService(Long accId, String key) {
        return mediaVerifyRepository.existsByAccntIdAndMediaValidString(accId, key);
    }

    public void emailVerifyCheckService(Long accId, String key) {


        EmailverifyDTO Emailverify = EmailverifyDTO
                .builder()
                .accntId(accId)
                .verifyYn("Y")
                .build();
        AccountBaseDTO accountBaseDTO = modelMapper.map(Emailverify, AccountBaseDTO.class);

        List<MediaVerifyBaseDTO> media = mediaVerifyRepository.findByAccntIdAndMediaValidString(accId, key)
                .stream().map(e -> modelMapper.map(e, MediaVerifyBaseDTO.class)).collect(Collectors.toList());

        logger.info("============= - {}", media.get(0).toString());
//        logger.info("=============accto - {}", accntEntity.toString());

        if (media.get(0).getExpiredDt().plusDays(7).isAfter(LocalDateTime.now())) {
            for (MediaVerifyBaseDTO m : media) {
                if (m.getMediaValidString().equals(key)) {
                    logger.info("if init");

//                accountRepository.AccountVerifyUpdate(accId,"y");
                    accountRepository.AccountRepositoryDynamicUpdate(accountBaseDTO);
                }
            }
        }
    }

    void emailLogSave(EmailSendRequest req, String verifyTypeCd, Long accountId, String verifyNum) {
        List<String> id = Arrays.asList(req.getReceiver());
        MediaVerify entity = MediaVerify
                .builder()
                .accntId(accountId)            //사용한 어카운트아이디
                .mediaVerifyReqId(id.get(0))   //검증요청아이디
                .mediaTypeCd("mail") //검증미디어유형
                .mediaValidString(verifyNum)   //검증번호
                .verifyTypeCd(verifyTypeCd)       //이메일검증
                .expiredDt(LocalDateTime.now())
                .regDt(LocalDateTime.now())
                .build();
        mediaVerifyRepository.save(entity);
    }

    public MediaVerify MobileLogSave(Long accountId, String phoneNumberString, String verifyNum) {
        MediaVerify entity = MediaVerify
                .builder()
                .accntId(accountId)            //사용한 어카운트아이디
                .mediaVerifyReqId(phoneNumberString)   //검증요청핸드폰번호
                .mediaTypeCd("Mobile") //검증미디어유형
                .mediaValidString(verifyNum)   //검증번호
                .verifyTypeCd("Mobile")       //이메일검증
                .expiredDt(LocalDateTime.now())
                .regDt(LocalDateTime.now())
                .build();
        mediaVerifyRepository.save(entity);
        return entity;
    }

    public MediaVerifyBaseDTO MobileLogSelect(Long accountId, String number) {
        MediaVerifyBaseDTO res = mediaVerifyRepository.findByAccntIdAndMediaValidStringAndMediaTypeCd(accountId, number, "Mobile")
                .map(e -> modelMapper.map(e, MediaVerifyBaseDTO.class)).get();
        logger.info("MobileSelect Service --------{}", res);
        return res;
    }

    public void emailTemplateChoiceService(EmailSendRequest esr) {
//        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
//        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        String mailForm = "error";
        String vn = VerifyNumber.verifyNumber();
        String[] emailGet = esr.getReceiver();

        AccountBaseDTO aa = accountRepository.findByAccntEmail(emailGet[0]).map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
        logger.info(" - {} ", aa);
        if (esr.getUseForm().equals("ResetPassword")) {
            List<String> email = Arrays.asList(esr.getReceiver());
            mailForm = mailTemplateReadService
                    .ResetPasswordTemplateRead("ResetPassword", email.get(0), vn);
            esr.setCustomerMessage(mailForm);
            emailLogSave(esr, "passEmail", aa.getAccntId(), vn);
            emailSendServiceMethod(esr);
        }

        if (esr.getUseForm().equals("UserCertification")) {
            String verifyEmail = esr.getReceiver().toString();
            StringBuffer sb = new StringBuffer();
            sb.append("/V1/emailcheck?");
            sb.append(String.format("accId=%s", aa.getAccntId()));
            sb.append(String.format("&key=%s", vn));
            mailForm = mailTemplateReadService
                    .UserCertificationReadTemplateRead("UserCertification", sb.toString());
            esr.setCustomerMessage(mailForm);
            emailLogSave(esr, "email", aa.getAccntId(), vn);
            emailSendServiceMethod(esr);
        }
    }

    public void emailSendServiceMethod(EmailSendRequest esr) {
        awsSesService.TextSendMail(
                esr.getReceiver(),
                devemail,
                esr.getCustomerMessage(),
                esr.getUseForm());
        logger.info("seccess");
    }

    public void emailCheckTemplate(EmailSendRequest esr, HttpServletRequest req, String tokenCity) {

    }

    public void PasswordResetService(EmailOrPhone dto) {
        char[] charChecl = dto.getEmailOrPhone().toCharArray();
        int tempCount = 0;
        AccountBaseDTO baseDTO = null;
        EmailSendRequest esr = new EmailSendRequest();
        for (char e : charChecl) {
            if (e == '@') {
                tempCount++;
                logger.info("@걸림 - - {}", tempCount);
            }
        }
        // 1이여야 이메일
        if (tempCount == 1) {
            int emailC = accountRepository.countByAccntEmail(dto.getEmailOrPhone());
            logger.info("EMAIL COUNT - {}", emailC);
            if (emailC > 0) {
                baseDTO = accountRepository.findByAccntEmail(dto.getEmailOrPhone())
                        .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
            }
        }
        //0이면핸드폰
        if (tempCount == 0) {
            int phoneC = accountRepository.countByPhoneNumberAndVerifyPhoneYn(dto.getEmailOrPhone(), "Y");
            logger.info("PHONE COUNT - {}", phoneC);
            if (phoneC > 0) {
                baseDTO = accountRepository.findByPhoneNumberAndVerifyPhoneYn(dto.getEmailOrPhone(), "Y")
                        .map(e -> modelMapper.map(e, AccountBaseDTO.class)).get();
                ;
            }
        }

        if (baseDTO == null || baseDTO.equals("") || baseDTO.equals(null)) {
            throw new CustomException("검색결과 없음", HttpStatus.BAD_REQUEST);
        } else {
            String[] emailA = {baseDTO.getAccntEmail()};
            esr.setReceiver(emailA);
            esr.setUseForm("ResetPassword");
        }
        emailTemplateChoiceService(esr);
    }

    public void PasswordValidService(EmailPasswordValid dto) {
        int count = mediaVerifyRepository.countByMediaVerifyReqIdAndMediaValidString(dto.getEmail(), dto.getNumber());
        if (count > 0) {
            MediaVerifyBaseDTO result = mediaVerifyRepository.findByMediaVerifyReqIdAndMediaValidString(dto.getEmail(), dto.getNumber())
                    .map(e -> modelMapper.map(e, MediaVerifyBaseDTO.class)).get();
            if (result.getExpiredDt().plusDays(30).isBefore(LocalDateTime.now())) {
                throw new CustomException("30일 경과", HttpStatus.BAD_REQUEST);
            }

            AccountBaseDTO baseDTO = AccountBaseDTO
                    .builder()
                    .accntId(result.getAccntId())
                    .accntPass(passwordEncoder.encode(dto.getPassword()))
                    .modUsr(result.getAccntId())
                    .modDt(LocalDateTime.now())
                    .build();
            accountRepository.AccountRepositoryDynamicUpdate(baseDTO);
        }

    }

    public void RSVPprivateInviteEmailService(RsvpSendForm form) {
        logger.info("sender init");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>SPARWK-Email</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"margin:0;padding: 0;\">\n" +
                "\n" +
                "    <div\n" +
                "        style=\"width:900px; box-sizing: border-box; font-family:'Malgun Gothic','맑은 고딕', sans-serif; letter-spacing:0px;\">\n" +
                "\n" +
                "        <div style=\"width:100%;\">\n" +
                "            <div style=\"width:100%; position: relative; text-align: center; background:#182566; margin:0;\n" +
                "\t\t\t\tbox-sizing: border-box; padding: 20px;\">\n" +
                "                <h1 style=\"font-size: 26px; font-weight: bold; color: #fff; margin:3px; margin-top: 10px;\">");

        sb.append("\n");
        sb.append(form.getOwnerName());
        sb.append("</h1>\n" +
                "                <h3\n" +
                "                    style=\"font-size: 22px; font-weight: normal; color: #fff; letter-spacing: -0.5px; margin: 0px; margin-bottom: 17px; line-height: 1.1;\">\n" +
                "                    You have been invited to the<br />following Project\n" +
                "                </h3>\n" +
                "                <p style=\"font-size: 14px; font-weight: normal; color: #fff; margin: 0px; margin-bottom: 10px;\">");
        sb.append(DateUtils.dateToString(DateUtils.now(), "dd MM yy / HH : mm"));
        sb.append("\n");


        sb.append(" </p>\n" +
                "                <p\n" +
                "                    style=\"font-size: 14px; font-weight: normal; color: #fff; line-height: 1.2; margin: 0px; margin-bottom: 10px;\">");
        if (form.getMemberName() == null
                || form.getMemberName().isEmpty()
                || form.getMemberName().equals("")
                || form.getMemberName().equals(null)
                || form.getMemberName().equals("null")
                || form.getMemberName().equals("[]")
                || form.getMemberName().equals("NULL")) {
        } else {
            sb.append(form.getMemberName());
            sb.append("<br />\n" +
                    "                    (");
            sb.append(form.getMemberCount());
            sb.append("Connections work here)\n");

        }

        if (form.getOwnerProfileImg() == null
                || form.getOwnerProfileImg().isEmpty()
                || form.getOwnerProfileImg().equals("")
                || form.getOwnerProfileImg().equals(null)) {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"margin-top: 14px;\" src=\"");
            sb.append("https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon_trans.png\"");
            sb.append("alt=\"photo_none\" />\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");
        } else {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"width: 100%;\" src=\"");
            sb.append(form.getOwnerProfileImg());
            sb.append("\" alt=\"photo\">\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");

        }
        sb.append(form.getOwnerName());
        sb.append("  </h5>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "\n" +
                "                    <p style=\"font-size: 16px; letter-spacing: 0px; margin:0px 0px 18px;\">");
        sb.append("\n");
        sb.append(form.getProjectDesctip());
        sb.append("</p>\n" +
                "                    <p style=\"font-size: 14px; letter-spacing: 0px; margin:0px 0px 15px;\">");
        sb.append("\n");
        sb.append(form.getProjectSubDesctip());
        sb.append(" </p>\n" +
                "\n" +
                "                    <div style=\"width: 100%; text-align: center; margin:60px 0px 0px;\">\n" +
                "                        <div style=\"margin:0px 0px 50px\">\n" +
                "                            <p style=\"margin:0px; font-size: 15px;\">\n" +
                "                                Password Protection\n" +
                "                            </p>\n" +
                "                            <h5\n" +
                "                                style=\"font-size: 24px; font-weight: bold; color: #ff994a; letter-spacing: 0px; margin: 0px;\">");
        sb.append(form.getProjectPassword());
        sb.append("\n");
        sb.append("</h5>\n" +
                "                        </div>\n" +
                "                        <div>");
        String inParam = String.format("%s?p1=%s&id=%s", invitation, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", inParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.naver.com").toString());
        sb.append(" <span style=\"width: 200px; display: inline-block; background: #ff6e19; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">RSVP NOW</span>\n" +
                "                            </a>\n" +
                "                        </div>\n" +
                "                        <div style=\"margin-top:15px;\">");
        String reParam = String.format("%s?p1=%s&id=%s", reject, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", reParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.google.com").toString());
//        sb.append("<span style=\"width: 200px; display: inline-block; background: #273ead; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
//                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">Reject</span>\n" );

        sb.append("</a>\n" +
                "                        </div>\n" +
                "                        <p\n" +
                "                            style=\"margin:0px; margin-top: 20px; font-size: 15px; font-weight: normal; color: #fff; letter-spacing: 0px;\">\n" +
                "                            Please proceed with the certification within 30 days.<br />\n" +
                "                            If you do not authenticate within 30 days, it will be treated as an unavailable account\n" +
                "                            and<br />\n" +
                "                            all information stored in your account will be deleted.\n" +
                "                        </p>\n" +
                "                    </div>\n" +
                "\n" +
                "                </div>\n" +
                "            </div>\n" +
                "\n" +
                "        </div>\n" +
                "\n" +
                "    </div>\n" +
                "\n" +
                "</body>\n" +
                "\n" +
                "</html>");
        String sub = String.format("Invitation : [SPARWK] %s @ {%s} %s", form.getProjectTitle(), form.getOwnerName(), form.getSenderEmail());
        String[] re = {form.getReceiverEmail()};
//        awsSesService.TextSendMail(re, form.getSenderEmail(), sb.toString(), sub);
        awsSesService.TextSendMail(re, devemail, sb.toString(), sub);
        logger.info("send clear");
    }

    public void RSVPprivateApplyEmailService(RsvpSendForm form) {
        logger.info("sender init");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>SPARWK-Email</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"margin:0;padding: 0;\">\n" +
                "\n" +
                "    <div\n" +
                "        style=\"width:900px; box-sizing: border-box; font-family:'Malgun Gothic','맑은 고딕', sans-serif; letter-spacing:0px;\">\n" +
                "\n" +
                "        <div style=\"width:100%;\">\n" +
                "            <div style=\"width:100%; position: relative; text-align: center; background:#182566; margin:0;\n" +
                "\t\t\t\tbox-sizing: border-box; padding: 20px;\">\n" +
                "                <h1 style=\"font-size: 26px; font-weight: bold; color: #fff; margin:3px; margin-top: 10px;\">");

        sb.append("\n");
        sb.append(form.getOwnerName());
        sb.append("</h1>\n" +
                "                <h3\n" +
                "                    style=\"font-size: 22px; font-weight: normal; color: #fff; letter-spacing: -0.5px; margin: 0px; margin-bottom: 17px; line-height: 1.1;\">\n" +
                "                    You have been accepted to the project.\n" +
                "                </h3>\n" +
                "                <p style=\"font-size: 14px; font-weight: normal; color: #fff; margin: 0px; margin-bottom: 10px;\">");
        sb.append(DateUtils.dateToString(DateUtils.now(), "dd MM yy / HH : mm"));
        sb.append("\n");


        sb.append(" </p>\n" +
                "                <p\n" +
                "                    style=\"font-size: 14px; font-weight: normal; color: #fff; line-height: 1.2; margin: 0px; margin-bottom: 10px;\">");
        if (form.getMemberName() == null
                || form.getMemberName().isEmpty()
                || form.getMemberName().equals("")
                || form.getMemberName().equals(null)
                || form.getMemberName().equals("null")
                || form.getMemberName().equals("[]")
                || form.getMemberName().equals("NULL")) {
        } else {
            sb.append(form.getMemberName());
            sb.append("<br />\n" +
                    "                    (");
            sb.append(form.getMemberCount());
            sb.append("Connections work here)\n");

        }

        if (form.getOwnerProfileImg() == null
                || form.getOwnerProfileImg().isEmpty()
                || form.getOwnerProfileImg().equals("")
                || form.getOwnerProfileImg().equals(null)) {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"margin-top: 14px;\" src=\"");
            sb.append("https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon_trans.png\"");
            sb.append("alt=\"photo_none\" />\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");
        } else {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"width: 100%;\" src=\"");
            sb.append(form.getOwnerProfileImg());
            sb.append("\" alt=\"photo\">\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");

        }
        sb.append(form.getOwnerName());
        sb.append("  </h5>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "\n" +
                "                    <p style=\"font-size: 16px; letter-spacing: 0px; margin:0px 0px 18px;\">");
        sb.append("\n");
        sb.append(form.getProjectDesctip());
        sb.append("</p>\n" +
                "                    <p style=\"font-size: 14px; letter-spacing: 0px; margin:0px 0px 15px;\">");
        sb.append("\n");
        sb.append(form.getProjectSubDesctip());
        sb.append(" </p>\n" +
                "\n" +
                "                    <div style=\"width: 100%; text-align: center; margin:60px 0px 0px;\">\n" +
                "                        <div style=\"margin:0px 0px 50px\">\n" +
                "                            <p style=\"margin:0px; font-size: 15px;\">\n" +
                "                                Password Protection\n" +
                "                            </p>\n" +
                "                            <h5\n" +
                "                                style=\"font-size: 24px; font-weight: bold; color: #ff994a; letter-spacing: 0px; margin: 0px;\">");
        sb.append(form.getProjectPassword());
        sb.append("\n");
        sb.append("</h5>\n" +
                "                        </div>\n" +
                "                        <div>");
        String inParam = String.format("%s?p1=%s&id=%s", invitation, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", inParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.naver.com").toString());
        sb.append(" <span style=\"width: 200px; display: inline-block; background: #ff6e19; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">RSVP NOW</span>\n" +
                "                            </a>\n" +
                "                        </div>\n" +
                "                        <div style=\"margin-top:15px;\">");

        sb.append(
                "                            </a>\n" +
                        "                        </div>\n" +
                        "                        <p\n" +
                        "                            style=\"margin:0px; margin-top: 20px; font-size: 15px; font-weight: normal; color: #fff; letter-spacing: 0px;\">\n" +
                        "                            Please proceed with the certification within 30 days.<br />\n" +
                        "                            If you do not authenticate within 30 days, it will be treated as an unavailable account\n" +
                        "                            and<br />\n" +
                        "                            all information stored in your account will be deleted.\n" +
                        "                        </p>\n" +
                        "                    </div>\n" +
                        "\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "\n" +
                        "        </div>\n" +
                        "\n" +
                        "    </div>\n" +
                        "\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>");
        String sub = String.format("Invitation : [SPARWK] %s @ {%s} %s", form.getProjectTitle(), form.getOwnerName(), form.getSenderEmail());
        String[] re = {form.getReceiverEmail()};
//        awsSesService.TextSendMail(re, form.getSenderEmail(), sb.toString(), sub);
        awsSesService.TextSendMail(re, devemail, sb.toString(), sub);
        logger.info("send clear");
    }

    public void RSVPpublicInviteEmailService(RsvpSendForm form) {
        logger.info("sender init");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>SPARWK-Email</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"margin:0;padding: 0;\">\n" +
                "\n" +
                "    <div\n" +
                "        style=\"width:900px; box-sizing: border-box; font-family:'Malgun Gothic','맑은 고딕', sans-serif; letter-spacing:0px;\">\n" +
                "\n" +
                "        <div style=\"width:100%;\">\n" +
                "            <div style=\"width:100%; position: relative; text-align: center; background:#182566; margin:0;\n" +
                "\t\t\t\tbox-sizing: border-box; padding: 20px;\">\n" +
                "                <h1 style=\"font-size: 26px; font-weight: bold; color: #fff; margin:3px; margin-top: 10px;\">");

        sb.append("\n");
        sb.append(form.getOwnerName());
        sb.append("</h1>\n" +
                "                <h3\n" +
                "                    style=\"font-size: 22px; font-weight: normal; color: #fff; letter-spacing: -0.5px; margin: 0px; margin-bottom: 17px; line-height: 1.1;\">\n" +
                "                    You have been invited to the<br />following Project\n" +
                "                </h3>\n" +
                "                <p style=\"font-size: 14px; font-weight: normal; color: #fff; margin: 0px; margin-bottom: 10px;\">");
        sb.append(DateUtils.dateToString(DateUtils.now(), "dd MM yy / HH : mm"));
        sb.append("\n");


        sb.append(" </p>\n" +
                "                <p\n" +
                "                    style=\"font-size: 14px; font-weight: normal; color: #fff; line-height: 1.2; margin: 0px; margin-bottom: 10px;\">");
        if (form.getMemberName() == null
                || form.getMemberName().isEmpty()
                || form.getMemberName().equals("")
                || form.getMemberName().equals(null)
                || form.getMemberName().equals("null")
                || form.getMemberName().equals("[]")
                || form.getMemberName().equals("NULL")) {
        } else {
            sb.append(form.getMemberName());
            sb.append("<br />\n" +
                    "                    (");
            sb.append(form.getMemberCount());
            sb.append("Connections work here)\n");

        }

        if (form.getOwnerProfileImg() == null
                || form.getOwnerProfileImg().isEmpty()
                || form.getOwnerProfileImg().equals("")
                || form.getOwnerProfileImg().equals(null)) {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"margin-top: 14px;\" src=\"");
            sb.append("https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon_trans.png\"");
            sb.append("alt=\"photo_none\" />\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");
        } else {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"width: 100%;\" src=\"");
            sb.append(form.getOwnerProfileImg());
            sb.append("\" alt=\"photo\">\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");

        }
        sb.append(form.getOwnerName());
        sb.append("  </h5>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "\n" +
                "                    <p style=\"font-size: 16px; letter-spacing: 0px; margin:0px 0px 18px;\">");
        sb.append("\n");
        sb.append(form.getProjectDesctip());
        sb.append("</p>\n" +
                "                    <p style=\"font-size: 14px; letter-spacing: 0px; margin:0px 0px 15px;\">");
        sb.append("\n");
        sb.append(form.getProjectSubDesctip());
        sb.append(" </p>\n" +
                "\n" +
                "                    <div style=\"width: 100%; text-align: center; margin:60px 0px 0px;\">\n" +
                "                        <div style=\"margin:0px 0px 50px\">\n" +
                "                            <p style=\"margin:0px; font-size: 15px;\">\n" +
                "                                \n" +
                "                            </p>\n" +
                "                            <h5\n" +
                "                                style=\"font-size: 24px; font-weight: bold; color: #ff994a; letter-spacing: 0px; margin: 0px;\">");
        sb.append("\n");
        sb.append("</h5>\n" +
                "                        </div>\n" +
                "                        <div>");
        String inParam = String.format("%s?p1=%s&id=%s", invitation, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", inParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.naver.com").toString());
        sb.append(" <span style=\"width: 200px; display: inline-block; background: #ff6e19; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">RSVP NOW</span>\n" +
                "                            </a>\n" +
                "                        </div>\n" +
                "                        <div style=\"margin-top:15px;\">");
        String reParam = String.format("%s?p1=%s&id=%s", reject, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", reParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.google.com").toString());
//        sb.append("<span style=\"width: 200px; display: inline-block; background: #273ead; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
//                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">Reject</span>\n" );
        sb.append("</a>\n" +
                "                        </div>\n" +
                "                        <p\n" +
                "                            style=\"margin:0px; margin-top: 20px; font-size: 15px; font-weight: normal; color: #fff; letter-spacing: 0px;\">\n" +
                "                            Please proceed with the certification within 30 days.<br />\n" +
                "                            If you do not authenticate within 30 days, it will be treated as an unavailable account\n" +
                "                            and<br />\n" +
                "                            all information stored in your account will be deleted.\n" +
                "                        </p>\n" +
                "                    </div>\n" +
                "\n" +
                "                </div>\n" +
                "            </div>\n" +
                "\n" +
                "        </div>\n" +
                "\n" +
                "    </div>\n" +
                "\n" +
                "</body>\n" +
                "\n" +
                "</html>");
        String sub = String.format("Invitation : [SPARWK] %s @ {%s} %s", form.getProjectTitle(), form.getOwnerName(), form.getSenderEmail());
        String[] re = {form.getReceiverEmail()};
//        awsSesService.TextSendMail(re, form.getSenderEmail(), sb.toString(), sub);
        awsSesService.TextSendMail(re, devemail, sb.toString(), sub);
        logger.info("send clear");
    }

    public void RSVPpublicApplyEmailService(RsvpSendForm form) {
        logger.info("sender init");
        StringBuffer sb = new StringBuffer();
        sb.append("<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>SPARWK-Email</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"margin:0;padding: 0;\">\n" +
                "\n" +
                "    <div\n" +
                "        style=\"width:900px; box-sizing: border-box; font-family:'Malgun Gothic','맑은 고딕', sans-serif; letter-spacing:0px;\">\n" +
                "\n" +
                "        <div style=\"width:100%;\">\n" +
                "            <div style=\"width:100%; position: relative; text-align: center; background:#182566; margin:0;\n" +
                "\t\t\t\tbox-sizing: border-box; padding: 20px;\">\n" +
                "                <h1 style=\"font-size: 26px; font-weight: bold; color: #fff; margin:3px; margin-top: 10px;\">");

        sb.append("\n");
        sb.append(form.getOwnerName());
        sb.append("</h1>\n" +
                "                <h3\n" +
                "                    style=\"font-size: 22px; font-weight: normal; color: #fff; letter-spacing: -0.5px; margin: 0px; margin-bottom: 17px; line-height: 1.1;\">\n" +
                "                    You have been accepted to the project.\n" +
                "                </h3>\n" +
                "                <p style=\"font-size: 14px; font-weight: normal; color: #fff; margin: 0px; margin-bottom: 10px;\">");
        sb.append(DateUtils.dateToString(DateUtils.now(), "dd MM yy / HH : mm"));
        sb.append("\n");


        sb.append(" </p>\n" +
                "                <p\n" +
                "                    style=\"font-size: 14px; font-weight: normal; color: #fff; line-height: 1.2; margin: 0px; margin-bottom: 10px;\">");
        if (form.getMemberName() == null
                || form.getMemberName().isEmpty()
                || form.getMemberName().equals("")
                || form.getMemberName().equals(null)
                || form.getMemberName().equals("null")
                || form.getMemberName().equals("[]")
                || form.getMemberName().equals("NULL")) {
        } else {
            sb.append(form.getMemberName());
            sb.append("<br />\n" +
                    "                    (");
            sb.append(form.getMemberCount());
            sb.append("Connections work here)\n");

        }

        if (form.getOwnerProfileImg() == null
                || form.getOwnerProfileImg().isEmpty()
                || form.getOwnerProfileImg().equals("")
                || form.getOwnerProfileImg().equals(null)) {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"margin-top: 14px;\" src=\"");
            sb.append("https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon_trans.png\"");
            sb.append("alt=\"photo_none\" />\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");
        } else {
            sb.append("                </p>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div style=\"width:100%; box-sizing: border-box; background:#0c1333;\">\n" +
                    "                <div\n" +
                    "                    style=\"font-size:15px; color: #fff; text-align: center; background-color: #0c1333; padding:50px 25px 65px;\">\n" +
                    "\n" +
                    "                    <div style=\"margin:0px; margin-bottom: 40px;\">\n" +
                    "                        <p style=\"margin: 0px; margin-bottom: 10px; font-size: 24px;\">Welcome to</p>\n" +
                    "                        <img src=\"https://sparwk-account.s3.ap-northeast-2.amazonaws.com/sparwk.email.send.icon.png\" alt=\"logo\" />\n" +
                    "                        <div style=\"margin:30px 0px 0px; text-align: center;\">\n" +
                    "                            <div style=\"width: 50px; height: 50px; margin: 0px auto; background: #0c1333; border: 2px solid #778eff; border-radius: 100%; \n" +
                    "                                text-align: center; overflow: hidden; position: relative;\">\n" +
                    "                                <img style=\"width: 100%;\" src=\"");
            sb.append(form.getOwnerProfileImg());
            sb.append("\" alt=\"photo\">\n" +
                    "                            </div>\n" +
                    "                            <h5\n" +
                    "                                style=\"font-size: 17px; font-weight: bold; color: #fff; letter-spacing: 0px; margin: 0px; margin: 7px 0px;\">");

        }
        sb.append(form.getOwnerName());
        sb.append("  </h5>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "\n" +
                "                    <p style=\"font-size: 16px; letter-spacing: 0px; margin:0px 0px 18px;\">");
        sb.append("\n");
        sb.append(form.getProjectDesctip());
        sb.append("</p>\n" +
                "                    <p style=\"font-size: 14px; letter-spacing: 0px; margin:0px 0px 15px;\">");
        sb.append("\n");
        sb.append(form.getProjectSubDesctip());
        sb.append(" </p>\n" +
                "\n" +
                "                    <div style=\"width: 100%; text-align: center; margin:60px 0px 0px;\">\n" +
                "                        <div style=\"margin:0px 0px 50px\">\n" +
                "                            <p style=\"margin:0px; font-size: 15px;\">\n" +
                "                                " +
                "                            </p>\n" +
                "                            <h5\n" +
                "                                style=\"font-size: 24px; font-weight: bold; color: #ff994a; letter-spacing: 0px; margin: 0px;\">");
        sb.append("\n");
        sb.append("</h5>\n" +
                "                        </div>\n" +
                "                        <div>");
        String inParam = String.format("%s?p1=%s&id=%s", invitation, form.getProjId().toString(), form.getRsvpProfileId().toString());
        sb.append(String.format("     <a href=\"%s\">", inParam.toString()));
//        sb.append(String.format("     <a href=\"%s\">", "https://www.naver.com").toString());
        sb.append(" <span style=\"width: 200px; display: inline-block; background: #ff6e19; border-radius:5px; padding:14px 40px 12px; color: #fff;\n" +
                "                            font-size: 16px; font-weight: bold; border: none; outline: none; cursor: pointer; text-decoration: none;\">RSVP NOW</span>\n" +
                "                            </a>\n" +
                "                        </div>\n" +
                "                        <div style=\"margin-top:15px;\">");

        sb.append(
                "                            </a>\n" +
                        "                        </div>\n" +
                        "                        <p\n" +
                        "                            style=\"margin:0px; margin-top: 20px; font-size: 15px; font-weight: normal; color: #fff; letter-spacing: 0px;\">\n" +
                        "                            Please proceed with the certification within 30 days.<br />\n" +
                        "                            If you do not authenticate within 30 days, it will be treated as an unavailable account\n" +
                        "                            and<br />\n" +
                        "                            all information stored in your account will be deleted.\n" +
                        "                        </p>\n" +
                        "                    </div>\n" +
                        "\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "\n" +
                        "        </div>\n" +
                        "\n" +
                        "    </div>\n" +
                        "\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>");
        String sub = String.format("Invitation : [SPARWK] %s @ {%s} %s", form.getProjectTitle(), form.getOwnerName(), form.getSenderEmail());
        String[] re = {form.getReceiverEmail()};
//        awsSesService.TextSendMail(re, form.getSenderEmail(), sb.toString(), sub);
        awsSesService.TextSendMail(re, devemail, sb.toString(), sub);
        logger.info("send clear");
    }

    @Value("${sparwk.node.schema}")
    private String schema;
    @Value("${sparwk.node.frontadminpass}")
    private String frontadminpass;

    public void AdminCreateSendEmailService(AdminEmailSendRequest dto) {

        String verifyNumberString = VerifyNumber.verifyNumber();


        mediaVerifyRepository.save(MediaVerify
                .builder()
                .accntId(Long.parseLong(dto.getContents()))
                .expiredDt(LocalDateTime.now().plusDays(30))
                .mediaTypeCd("ADMIN")
                .mediaValidString(verifyNumberString)
                .mediaVerifyReqId(dto.getReceiver()[0])
                .regDt(LocalDateTime.now())
                .verifyTypeCd("email")
                .build());
        StringBuffer sb = new StringBuffer();
        sb.append("<html>\n" +
                "\n" +
                "<head>\n" +
                "    <title>SPARWK-Email</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "</head>\n" +
                "\n" +
                "<body style=\"margin:0;padding: 0;\">\n" +
                "\n" +
                "<div\n" +
                "        style=\"width:900px; box-sizing: border-box; font-family:'Malgun Gothic','맑은 고딕', sans-serif; letter-spacing: -0.5px; border: 1px solid #e1e1e1;\">\n" +
                "\n" +
                "    <div style=\"width:100%;\">\n" +
                "        <div style=\"width:100%; height:100px; position: relative; text-align: center; background:#fff; border-top:8px solid #fff; margin:0;\n" +
                "\t\t\t\tbox-sizing: border-box; padding: 20px; padding-bottom: 10px;\">\n" +
                "            <h1 style=\"font-size: 30px; font-weight: bold; color: #2a7bf3; margin: 0px; margin-top:7px;\">\n" +
                "                Welcome to SPARWK\n" +
                "            </h1>\n" +
                "        </div>\n" +
                "\n" +
                "        <div style=\"width:100%; box-sizing: border-box; background:#fff;\">\n" +
                "            <div style=\"font-size:16px; color: #555; letter-spacing: -1px; background-color: #fff; padding: 25px;\">\n" +
                "                <p style=\"margin:0px 0px 10px;\">\n" +
                "                    Sparwk has registered you as an administrator.<br />\n" +
                "                    You need to set a password for the first login.<br />\n" +
                "                </p>\n" +
                "                <p style=\"margin:0px 0px 10px;\">\n" +
                "                    Click the Reset Password Button to go to the settings page.<br />\n" +
                "                    <br />\n" +
                "                </p>\n" +
                "                <p style=\"margin:0px 0px 10px;\">\n" +
                "                    Resistered Email Address:");

        sb.append(dto.getReceiver()[0]);
        sb.append("\n" +
                "                </p>\n" +
                "                <div style=\"width: 100%; text-align: center; margin:50px 0px 40px;\">\n");
        sb.append(String.format("                    <a href='%s'", schema+"://" + mailing+"/V1/adminMail" + "?accId=" + dto.getContents() + "&key=" + verifyNumberString));
        logger.info("url ---------{}", schema+"://" + mailing + "?accId=" + dto.getContents() + "&key=" + verifyNumberString);
        sb.append(" style=\"background: #2a7bf3; border-radius:3px; padding:7px 12px; color: #fff; border: none; outline: none; font-size: 14px; cursor: pointer;\">\n" +
                "                        Reset Password\n" +
                "                    </a>\n" +
                "                        <p\n" +
                "                            style=\"margin:0px; margin-top: 40px; font-size: 16px; font-weight: bold; color: #222; line-height: 1.4; letter-spacing: 0px;\">\n" +
                "                        Make your dreams come true with SPARWK.\n" +
                "                    </p>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "\n" +
                "</html>");

        String sub = "Sparwk Admin Password Update & Reset";
//        awsSesService.TextSendMail(re, form.getSenderEmail(), sb.toString(), sub);
        awsSesService.TextSendMail(dto.getReceiver(), devemail, sb.toString(), sub);
        logger.info("send clear");
    }


}
