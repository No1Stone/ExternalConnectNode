package com.sparwk.externalconnectnode.mailing.jpa.dto;

import lombok.*;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter@Setter
@Builder
@NoArgsConstructor@AllArgsConstructor
@ToString
public class AccountBaseDTO {

    private Long accntId;

    private Long accntRepositoryId;

    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String accntTypeCd;

    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;

    @Size(max = 200, message = "encoding")
    private String accntPass;

    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String countryCd;

    @Size(max = 20, message = "")
    private String phoneNumber;

    @Size(max = 1, message = "y or n")
    private String accntLoginActiveYn;

    private Long lastUseProfileId;

    @Size(max = 1, message = "y or n")
    private String verifyYn;
    @Size(max = 1, message = "y or n")
    private String useYn;
    @Size(max = 1, message = "y or n")
    private String verifyPhoneYn;
    @Size(max = 1, message = "y or n")
    private String receiveMarketingInfoYn;
    @Size(max = 1, message = "y or n")
    private String provideInfoTothirdYn;
    @Size(max = 1, message = "y or n")
    private String personalInfoCollectionYn;
    @Size(max = 1, message = "y or n")
    private String provideInfoTosparwkYn;
    @Size(max = 1, message = "y or n")
    private String transferInfoToabroadYn;


    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;


}
