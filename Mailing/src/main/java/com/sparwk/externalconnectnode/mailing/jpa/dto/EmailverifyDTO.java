package com.sparwk.externalconnectnode.mailing.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmailverifyDTO {

    private Long accntId;
    private String verifyYn;

}
