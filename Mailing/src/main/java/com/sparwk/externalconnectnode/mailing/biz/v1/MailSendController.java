package com.sparwk.externalconnectnode.mailing.biz.v1;

import com.sparwk.externalconnectnode.mailing.biz.v1.dto.RsvpSendForm;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.SESServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/mail")
@CrossOrigin("*")
public class MailSendController {

    private final Logger logger = LoggerFactory.getLogger(MailSendController.class);
    @Autowired
    private SESServiceRepository sesServiceRepository;

    @PostMapping(path = "/RSVP/private/invite")
    public void RSVPprivateInviteEmailController(@RequestBody RsvpSendForm form) {
        logger.info("--------{}", form);
        sesServiceRepository.RSVPprivateInviteEmailService(form);
    }

    @PostMapping(path = "/RSVP/private/apply")
    public void RSVPprivateApplyEmailController(@RequestBody RsvpSendForm form) {
        logger.info("--------{}", form);
        sesServiceRepository.RSVPprivateApplyEmailService(form);
    }

    @PostMapping(path = "/RSVP/public/invite")
    public void RSVPpublicInviteEmailController(@RequestBody RsvpSendForm form) {
        logger.info("--------{}", form);
        sesServiceRepository.RSVPpublicInviteEmailService(form);
    }

    @PostMapping(path = "/RSVP/public/apply")
    public void RSVPpublicApplyEmailController(@RequestBody RsvpSendForm form) {
        logger.info("--------{}", form);
        sesServiceRepository.RSVPpublicApplyEmailService(form);
    }


}
