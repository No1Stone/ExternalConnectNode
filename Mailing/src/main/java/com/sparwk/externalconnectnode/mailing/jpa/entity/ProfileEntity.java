package com.sparwk.externalconnectnode.mailing.jpa.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
@Getter
@NoArgsConstructor
public class ProfileEntity {

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 4, max = 200)
    private String profileId;

    @Builder
    ProfileEntity(Long id, String profileId){
    this.id = id;
    this.profileId = profileId;
    }

}
