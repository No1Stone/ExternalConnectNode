package com.sparwk.externalconnectnode.mailing.biz.v1.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class MailTemplateReadService {
    private final Logger logger = LoggerFactory.getLogger(MailTemplateReadService.class);

    @Value("${sparwk.node.project.projectip}")
    private String projectIp;

    @Value("${sparwk.node.personalization.accountip}")
    private String accountIp;

    @Value("${sparwk.node.personalization.authip}")
    private String authIp;

    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;

    @Value("${sparwk.node.externalconnect.mailingip}")
    private String mailingIp;

    @Value("${sparwk.node.frontupdatepass}")
    private String frontURL;


    public String UserCertificationReadTemplateRead(String templateName, String emailText) {
        StringBuffer result = new StringBuffer();
        StringBuffer dirPath = new StringBuffer();
        dirPath.append("/mail_template/");
        dirPath.append(templateName);
        dirPath.append(".html");
        ClassPathResource cpr = new ClassPathResource(dirPath.toString());
        logger.info(" cpr pathurl - {}", cpr.toString());
        logger.info("email text - {}",emailText);
            try {
                Path path = Paths.get(cpr.getURI());
                List<String> content = Files.readAllLines(path);
                content.forEach(System.out::println);
                for (String e : content) {
                    if (e.startsWith("                    <a href=\"http://\"")) {
                        e = String.format("\"                    <a href=\"http://%s%s\" style=background: #2a7bf3; border-radius:3px; padding:7px 12px; color: #fff; border: none; outline: none; font-size: 14px; cursor: pointer;>",mailingIp, emailText);
                        logger.info("================= - {}", e.toString());
                        logger.info("================= - {}", emailText);
                    }
                    result.append(e);
                }
                logger.info("append - {}", result.toString());
            } catch (IOException e) {
                logger.error(" - {}", e.getMessage(), e);
            }
        return result.toString();
    }


    public String ResetPasswordTemplateRead(String templateName, String emailText, String vn){
        StringBuffer result = new StringBuffer();
        StringBuffer dirPath = new StringBuffer();
        dirPath.append("/mail_template/");
        dirPath.append(templateName);
        dirPath.append(".html");
        ClassPathResource cpr = new ClassPathResource(dirPath.toString());
        StringBuffer buttonSet = new StringBuffer();
        buttonSet.append(frontURL);
        buttonSet.append("?email=");
        buttonSet.append(emailText);
        buttonSet.append("&number=");
        buttonSet.append(vn);

        logger.info("button ToStiong  ------------- - {}",buttonSet.toString());

        logger.info(" cpr pathurl - {}", cpr.toString());
        logger.info("email text - {}",emailText);
        try {
            Path path = Paths.get(cpr.getURI());
            List<String> content = Files.readAllLines(path);
            content.forEach(System.out::println);
            for (String e : content) {
                if (e.startsWith("                    Resistered Email Address:")) {
                    e = String.format("<a style=\"color:#ed6515; margin-left:5px;\" href=\"\">Email : your %s </a>"
                            , emailText);
                    logger.info("================= - {}", e.toString());
                    logger.info("================= - {}", emailText);
                }

                if (e.startsWith("                    <a href=''")) {
//                    e = String.format("                    <a href='%s?email=%s'",frontURL,emailText);
                    e = String.format("                    <a href='%s'",buttonSet.toString());
                    logger.info("================= - {}", e.toString());
                    logger.info("================= - {}", emailText);
                }

                result.append(e);
            }
            logger.info("append - {}", result.toString());
        } catch (IOException e) {
            logger.error(" - {}", e.getMessage(), e);
        }
        return result.toString();
    }


}
