package com.sparwk.externalconnectnode.mailing.biz.v1;

import com.sparwk.externalconnectnode.mailing.biz.v1.dto.EmailOrPhone;
import com.sparwk.externalconnectnode.mailing.biz.v1.dto.EmailPasswordValid;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.SESServiceRepository;
import com.sparwk.externalconnectnode.mailing.jpa.repository.ProfileRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/password")
@RequiredArgsConstructor
@Api(tags = "Mailing Server password reset")
@CrossOrigin("*")
public class PublicController {

	private final Logger logger = LoggerFactory.getLogger(PublicController.class);

	@Autowired
	private SESServiceRepository sesServiceRepository ;
	@Autowired
	private ProfileRepository profileRepository;
	@ApiOperation(
			value = "핸드폰이나, 이메일주소를 입력하는 에이피아이"
	)
	@Operation(
			description = "프론트의 비밀번호 입력페이지로 리다이렉션시킴",
			parameters = {
					@Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "메일보내기")
			}
	)
	@PostMapping(path = "/reset")
	public void PasswordResetController(@RequestBody EmailOrPhone emailOrPhone){
		sesServiceRepository.PasswordResetService(emailOrPhone);
	}

	@ApiOperation(
			value = "위에 에이피 아이를 사용하여 받은 정보를 셋팅하여 비밀번호를 변경"
	)
	@Operation(
			description = "템플릿 ResetPassword  /  UserCertification",
			parameters = {
					@Parameter(name = "dto", in = ParameterIn.QUERY, required = true, description = "메일보내기")
			}
	)
	@PostMapping(path = "/vaild")
	public void PasswordVaildController(@RequestBody EmailPasswordValid dto){
		sesServiceRepository.PasswordValidService(dto);
	}

}
