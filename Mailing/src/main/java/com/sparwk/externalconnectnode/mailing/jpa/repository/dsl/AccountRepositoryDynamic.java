package com.sparwk.externalconnectnode.mailing.jpa.repository.dsl;

import com.sparwk.externalconnectnode.mailing.jpa.dto.AccountBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface AccountRepositoryDynamic {
    @Transactional
    Long AccountRepositoryDynamicUpdate(AccountBaseDTO entity);

    @Transactional
    Long AccountVerifyUpdate(Long acc, String yn);

}
