package com.sparwk.externalconnectnode.mailing.biz.v1;

import com.sparwk.externalconnectnode.mailing.biz.v1.dto.AdminEmailSendRequest;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.AwsSesService;
import com.sparwk.externalconnectnode.mailing.biz.v1.service.SESServiceRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1/admin")
@CrossOrigin("*")
@RequiredArgsConstructor
public class AdminSendEmail {

    private final Logger logger = LoggerFactory.getLogger(AdminSendEmail.class);

    private final AwsSesService awsSesService;
    private final SESServiceRepository sesServiceRepository;

    @PostMapping(path = "/send")
    public void testEmailSend(@RequestBody AdminEmailSendRequest dto){
        logger.info("========={}",dto);
        awsSesService.TextSendMail(dto.getReceiver(),"admin@sparwkdev.com", dto.getContents(),dto.getSubject());
    }

    @PostMapping(path = "/create")
    public void AdminCreateSendEmailController(@RequestBody AdminEmailSendRequest dto){
        logger.info("========={}",dto);
        sesServiceRepository.AdminCreateSendEmailService(dto);
    }

}
