package com.ui.sparwk.service;

import com.amazonaws.services.s3.AmazonS3;
import com.ui.sparwk.dto.S3FileBaseDTO;
import com.ui.sparwk.entity.S3File;
import com.ui.sparwk.repository.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class S3ServiceRepository {
    private final Logger logger = LoggerFactory.getLogger(S3ServiceRepository.class);

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AmazonS3 amazonS3;
    @Autowired
    private S3FileRepository s3BannerRepository;

    public List<S3FileBaseDTO> S3FileSelectServiceBucUseCom(String bucket, String useUniq, String component) {
        return s3BannerRepository.findByBucketTypeAndUseUniqueIdAndComponType(bucket, useUniq, component)
                .stream().map(e -> modelMapper.map(e, S3FileBaseDTO.class)).collect(Collectors.toList());
    }

    public void S3FileSaveService(S3FileBaseDTO DTO) {
        S3File entity = S3File
                .builder()
                .fileSeq(DTO.getFileSeq())
                .fileName(DTO.getFileName())
                .fileExetens(DTO.getFileExetens())
                .filePath(DTO.getFilePath())
                .fileSize(DTO.getFileSize())
                .bucketType(DTO.getBucketType())
                .useUniqueId(DTO.getUseUniqueId())
                .componType(DTO.getComponType())
                .createDt(DTO.getCreateDt())
                .frontSeq(DTO.getFrontSeq())
                .fileType(DTO.getFileType())
                .sendUrl(DTO.getSendUrl())
                .useYn(DTO.getUseYn())
                .keepPeriod(DTO.getKeepPeriod())
                .delYn(DTO.getDelYn())
                .build();
        s3BannerRepository.save(entity);
    }

    public void s3BannerRepositoryDynamicUpdate(S3FileBaseDTO DTO) {
        s3BannerRepository.S3FileRepositoryDynamicUpdate(DTO);
    }

}
