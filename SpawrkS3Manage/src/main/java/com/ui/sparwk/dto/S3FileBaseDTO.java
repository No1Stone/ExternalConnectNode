package com.ui.sparwk.dto;

import lombok.Setter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
@ToString
public class S3FileBaseDTO {

    private Long fileSeq;
    private String fileName;
    private String fileExetens;
    private String filePath;
    private String fileSize;
    private String bucketType;
    private Long useUniqueId;
    private String componType;
    private String createDt;
    private Long frontSeq;
    private String fileType;
    private String sendUrl;
    private String useYn;
    private LocalDateTime keepPeriod;
    private String delYn;

}
