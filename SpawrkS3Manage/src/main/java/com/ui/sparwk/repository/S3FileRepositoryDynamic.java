package com.ui.sparwk.repository;

import com.ui.sparwk.dto.S3FileBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface S3FileRepositoryDynamic {

    @Transactional
    void S3FileRepositoryDynamicUpdate(S3FileBaseDTO entity);

}
