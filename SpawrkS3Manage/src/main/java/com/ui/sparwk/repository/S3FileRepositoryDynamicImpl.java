package com.ui.sparwk.repository;

import com.ui.sparwk.dto.S3FileBaseDTO;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class S3FileRepositoryDynamicImpl implements S3FileRepositoryDynamic {
    @Override
    public void S3FileRepositoryDynamicUpdate(S3FileBaseDTO entity) {

    }
/*
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(S3FileRepositoryDynamicImpl.class);
    private QS3File qS3File = QS3File.s3File;

    private S3FileRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public void S3FileRepositoryDynamicUpdate(S3FileBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qS3File);

        if (entity.getFileSeq() != null) {
            jpaUpdateClause.set(qS3File.fileSeq, entity.getFileSeq());
        }
        if (!entity.getFileName().isEmpty()) {
            jpaUpdateClause.set(qS3File.fileName, entity.getFileName());
        }
        if (!entity.getFileExetens().isEmpty()) {
            jpaUpdateClause.set(qS3File.fileExetens, entity.getFileExetens());
        }
        if (!entity.getFilePath().isEmpty()) {
            jpaUpdateClause.set(qS3File.filePath, entity.getFilePath());
        }
        if (!entity.getFileSize().isEmpty()) {
            jpaUpdateClause.set(qS3File.fileSize, entity.getFileSize());
        }
        if (!entity.getCreateDt().isEmpty()) {
            jpaUpdateClause.set(qS3File.createDt, entity.getCreateDt());
        }
        if (entity.getFrontSeq() != null) {
            jpaUpdateClause.set(qS3File.frontSeq, entity.getFrontSeq());
        }
        if (!entity.getFileType().isEmpty()) {
            jpaUpdateClause.set(qS3File.fileType, entity.getFileType());
        }
        if (!entity.getSendUrl().isEmpty()) {
            jpaUpdateClause.set(qS3File.sendUrl, entity.getSendUrl());
        }
        if (!entity.getUseYn().isEmpty()) {
            jpaUpdateClause.set(qS3File.useYn, entity.getUseYn());
        }
        if (entity.getKeepPeriod() != null) {
            jpaUpdateClause.set(qS3File.keepPeriod, entity.getKeepPeriod());
        }
        if (!entity.getDelYn().isEmpty()) {
            jpaUpdateClause.set(qS3File.delYn, entity.getDelYn());
        }


        if (!entity.getBucketType().isEmpty()) {
            jpaUpdateClause.where(qS3File.bucketType.eq(entity.getBucketType()));
        }
        if (entity.getUseUniqueId() != null) {
            jpaUpdateClause.where(qS3File.useUniqueId.eq(entity.getUseUniqueId()));
        }
        if (!entity.getComponType().isEmpty()) {
            jpaUpdateClause.where(qS3File.componType.eq(entity.getComponType()));
        }

        jpaUpdateClause.execute();
        em.flush();
        em.clear();
    }
*/
}
