package com.ui.sparwk.repository;

import com.ui.sparwk.dto.S3FileBaseDTO;
import com.ui.sparwk.entity.S3File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface S3FileRepository extends JpaRepository<S3File, Long>, S3FileRepositoryDynamic
 {

  List<S3FileBaseDTO> findByBucketTypeAndUseUniqueIdAndComponType
          (String BucketType, String UseUniqueId, String ComponType);

}
