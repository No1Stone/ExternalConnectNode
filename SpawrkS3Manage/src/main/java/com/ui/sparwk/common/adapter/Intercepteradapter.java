package com.ui.sparwk.common.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ui.sparwk.common.util.ClientIp;
import com.ui.sparwk.common.util.ZoneUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class Intercepteradapter implements HandlerInterceptor {


    @Autowired
    private ClientIp clientIp;

    private final Logger logger = LoggerFactory.getLogger(Intercepteradapter.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
          /*
        if(request.getHeader("Authorization")==null){
            logger.info("token value null");
            return false;
        }

        String jwtToken = request.getHeader("Authorization").substring(7);
        //------------ split JWT ------------"
        String[] split_string = jwtToken.split("\\.");
        String base64EncodedHeader = split_string[0];
        String base64EncodedBody = split_string[1];
        String base64EncodedSignature = split_string[2];
        //------------ JWT Header decoding ------------"
        Base64 base64Url = new Base64(true);
        String header = new String(base64Url.decode(base64EncodedHeader));
        //------------ JWT Body decoding------------
        String body = new String(base64Url.decode(base64EncodedBody));
        //------------ JWT Signature decoding------------
        String Signature = new String(base64Url.decode(base64EncodedSignature));

        return HandlerInterceptor.super.preHandle(request, response, handler);
        */
        Long accid = null;
        if(request.getHeader("AccountId").isEmpty() || request.getHeader("AccountId")==null){
            accid = 0L;
        }
        else {
            accid = Long.parseLong(request.getHeader("AccountId"));
        }
        logger.info(" accRequest value check - {}",accid);
        ObjectMapper om = new ObjectMapper();
//        TokenBodyDTO tb = om.readValue(body, TokenBodyDTO.class);
        TokenBodyDTO tb = new TokenBodyDTO();
        //logger.info("intercepter pre");
        //logger.info("header - {}", header);
        //logger.info("body - {}", body);
        //logger.info("Signature - {}", Signature);
        //logger.info("tb info - {}",tb);
        //logger.info("tb info - {}",tb.getSub());
        tb.setUserIp(clientIp.getClientIp(request));
        tb.setUtc0(ZoneUtils.UTC0());
        //유저 토큰 utc terr 정보를 넣어서 셋
        //tb.setUserIp(ZoneUtils.cumstum());
        tb.setAccountId(accid);
        request.setAttribute("TokenBody", tb);
        return HandlerInterceptor.super.preHandle(request, response, handler);
//        return true;

    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
