package com.ui.sparwk.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_s3_file")
@Getter
@NoArgsConstructor
public class S3File extends BaseEntity {

    @Id
    @Column(name = "file_seq", nullable = true)
    private Long fileSeq;
    @Column(name = "file_name", nullable = true)
    private String fileName;
    @Column(name = "file_exetens", nullable = true)
    private String fileExetens;
    @Column(name = "file_path", nullable = true)
    private String filePath;
    @Column(name = "file_size", nullable = true)
    private String fileSize;
    @Column(name = "bucket_type", nullable = true)
    private String bucketType;
    @Column(name = "use_unique_id", nullable = true)
    private Long useUniqueId;
    @Column(name = "compon_type", nullable = true)
    private String componType;
    @Column(name = "create_dt", nullable = true)
    private String createDt;
    @Column(name = "front_seq", nullable = true)
    private Long frontSeq;
    @Column(name = "file_type", nullable = true)
    private String fileType;
    @Column(name = "send_url", nullable = true)
    private String sendUrl;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "keep_period", nullable = true)
    private LocalDateTime keepPeriod;
    @Column(name = "del_yn", nullable = true)
    private String delYn;

    @Builder
    private S3File(
            Long fileSeq,
            String fileName,
            String fileExetens,
            String filePath,
            String fileSize,
            String bucketType,
            Long useUniqueId,
            String componType,
            String createDt,
            Long frontSeq,
            String fileType,
            String sendUrl,
            String useYn,
            LocalDateTime keepPeriod,
            String delYn
            ) {
        this.fileSeq=fileSeq;
        this.fileName=fileName;
        this.fileExetens=fileExetens;
        this.filePath=filePath;
        this.fileSize=fileSize;
        this.bucketType=bucketType;
        this.useUniqueId=useUniqueId;
        this.componType=componType;
        this.createDt=createDt;
        this.frontSeq=frontSeq;
        this.fileType=fileType;
        this.sendUrl=sendUrl;
        this.useYn=useYn;
        this.keepPeriod=keepPeriod;
        this.delYn=delYn;
    }


}
