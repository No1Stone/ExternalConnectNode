package com.ui.sparwk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
public class AWSS3Config {
	
	@Value("${cloud.aws.credentials.accessKey}")
	private String accessKeyId;
	
	@Value("${cloud.aws.credentials.secretKey}")
	private String secretAccessKey;
	
	@Value("${cloud.aws.credentials.region}")
	private String region;
	
	@Bean
	public AmazonS3 getAmazonS3Client() {
		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
		return AmazonS3ClientBuilder
				.standard()
				.withRegion(Regions.AP_NORTHEAST_2)
				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
				.build();
	}



	//@Bean AmazonSimpleEmailServiceClients
	
}
