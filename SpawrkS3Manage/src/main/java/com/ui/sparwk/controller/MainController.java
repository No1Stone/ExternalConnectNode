package com.ui.sparwk.controller;

import com.ui.sparwk.dto.S3FileBaseDTO;
import com.ui.sparwk.service.S3AWSService;
import com.ui.sparwk.service.S3ServiceRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(path = "/sparwk/V1/user")
@RequiredArgsConstructor
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private S3ServiceRepository s3ServiceRepository;

    @Autowired
    private S3AWSService s3AWSService;

    @GetMapping(path = "/test1/{aa}")
    public void presignTest1(@PathVariable(name = "aa")String aa)throws IOException  {
        String bb = "bb";
        logger.info("================={}",aa);
        s3AWSService.aa(aa, "sparwk-account");
    }

    @GetMapping(path = "/test2")
    public void presignTest2() throws IOException {
        String aa = "aa";
        String bb = "bb";
        logger.info("=================");
        s3AWSService.bb(aa, "sparwk-account");
    }



//    @GetMapping(path = "/S3Select")
//    public List<S3FileBaseDTO> s3FileBaseDTOSelectController() {
//
//    }



}
