package com.ui.sparwk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpawrkS3ManageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpawrkS3ManageApplication.class, args);
	}

}
