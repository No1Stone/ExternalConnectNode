package com.example.tests3server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestS3ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestS3ServerApplication.class, args);
    }

}
