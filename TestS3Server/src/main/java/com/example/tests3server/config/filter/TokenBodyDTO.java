package com.example.tests3server.config.filter;

import lombok.*;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class TokenBodyDTO {
    String sub;
    long acclountId;
    String permissions;
    String createTime;
    String iat;
    String exp;
    String userIp;
    LocalDateTime utc0;
    LocalDateTime utcUser;
}
