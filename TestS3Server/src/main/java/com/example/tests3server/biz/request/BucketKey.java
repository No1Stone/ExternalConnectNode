package com.example.tests3server.biz.request;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class BucketKey {

    private String key;
    private String bucket;
}
