# **Development environment configuration**

```language JAVA
FrameWork 
  Spring Boot

Server Description 
  AWS SES BackEndServer

Server Spec
  Spring Mvc
  Spring Validation
  Spring DevTool
  Spring Web
  Spring Fox boot
  Spring Fox Swagger
  Spring Data JPA
  JDBC
  QueryDSL
  Lombok
  Log4j
  PostGesql
  AWS SDK SES
 ```
# **Server Architecture**
```
com.uinetwork.sparwk
├── common
│   ├─ adapter: Server Token decoding
│   └─ util: UtilCode
├── config: Aws/Database/Swageer Config
├── controller: API List
├── dto: Server Request, Response Entity
├── entity: Database Entity
├── repository: Database Query
├── response: RestAPI Response form
├── service: Sparwk Service 
└── validation: Sparwk base Role

├── qfile: Dabase QueryEntity
```

  
  