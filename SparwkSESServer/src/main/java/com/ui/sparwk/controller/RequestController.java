package com.ui.sparwk.controller;

import com.ui.sparwk.service.SESServiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/V1")
public class RequestController {
    private final Logger logger = LoggerFactory.getLogger(RequestController.class);


    @Autowired
    private SESServiceRepository sesServiceRepository;

    @RequestMapping(path = "/emailcheck"
    ,method = RequestMethod.GET
            )
    public String emailcheckinit(@RequestParam(value = "accId") String accId,
                                 @RequestParam(value = "key") String key) {
        logger.info(accId);
        logger.info(key);


        sesServiceRepository.emailVerifyCheckService(Long.parseLong(accId), key);

        return "https://www.naver.com";
    }

}
