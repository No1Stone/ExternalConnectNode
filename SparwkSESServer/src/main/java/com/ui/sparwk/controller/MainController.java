package com.ui.sparwk.controller;

import com.ui.sparwk.dto.email.EmailSendRequest;
import com.ui.sparwk.repository.profile.ProfileRepository;
import com.ui.sparwk.service.SESServiceRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/V1")
@RequiredArgsConstructor
public class MainController {

	private final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	private SESServiceRepository sesServiceRepository ;
	@Autowired
	private ProfileRepository profileRepository;

	/*
	@GetMapping(path = "/getip/{path}")
	public String ipget(@PathVariable(name = "path")String path, HttpServletRequest request){
		ZonedDateTime aa = ZonedDateTime.now(ZoneId.of("UTC+0"));
		logger.info("아이피 - {}",aa);
		logger.info("아이피 - {}",  LocalDateTime.now(ZoneId.of("UTC+0")));
		return "ok";
	}*/
	/*
	@GetMapping(path = "/get1/{path}")
	public Optional<EmailCheckRequest> DatabaseConnectTestGet1(@PathVariable(name = "path")String path ){
		logger.info("path value - {}", path);
		Long a = Long.parseLong(path);
		return sesServiceRepository.testget(a);
	}
	@GetMapping(path = "/get2/{path}")
	public Optional<EmailCheckRequest> DatabaseConnectTestGet2(@PathVariable(name = "path")String path ){
		logger.info("path value - {}", path);
		Long a = Long.parseLong(path);
		return sesServiceRepository.testget2(a);
	}
	@PostMapping(path = "/post1")
	public void DatabaseConnectTestPost1(@RequestBody EmailCheckRequest entity ){
	logger.info("entity value - {}",entity);
		sesServiceRepository.testpost(entity);
	}
*/



	@GetMapping(path = "/EmailOverlapCheck/{checkEmail}")
	public boolean EmailverifyController(
			@PathVariable(name = "checkEmail") String checkEmail,
			HttpServletRequest req) {
		if(profileRepository.findByProfileId(checkEmail).equals(checkEmail)){
			return false;
		}
	return true;
	}

	@PostMapping(path = "/sendEmail")
	public void SendEmailTemplate(@RequestBody EmailSendRequest entity, HttpServletRequest req){
		logger.info("entity - {} ",entity);
		sesServiceRepository.emailTemplateChoiceService(entity);
	}


}
