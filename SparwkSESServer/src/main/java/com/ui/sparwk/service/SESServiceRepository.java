package com.ui.sparwk.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.sparwk.common.util.ClientIp;
import com.ui.sparwk.common.util.VerifyNumber;
import com.ui.sparwk.dto.account.AccountBaseDTO;
import com.ui.sparwk.dto.account.EmailverifyDTO;
import com.ui.sparwk.dto.email.EmailSendRequest;
import com.ui.sparwk.dto.email.MediaVerifyBaseDTO;
import com.ui.sparwk.entity.email.MediaVerify;
import com.ui.sparwk.repository.account.AccountRepository;
import com.ui.sparwk.repository.email.EmailSendRepository;
import com.ui.sparwk.repository.email.MediaVerifyRepository;
import com.ui.sparwk.repository.profile.ProfileRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SESServiceRepository {

    private final Logger logger = LoggerFactory.getLogger(SESServiceRepository.class);

    private EntityManager em;
    private JPAQueryFactory queryFactory;

    private SESServiceRepository(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    //test repository
    @Autowired
    private EmailSendRepository emailSendRepository;
    @Autowired
    private ClientIp clientIp;
    @Autowired
    private MailTemplateReadService mailTemplateReadService;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private AwsSesService awsSesService;

    @Autowired
    private MediaVerifyRepository mediaVerifyRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${cloud.aws.devmail}")
    private String devemail;


    public void emailVerifyCheckService(Long accId, String key) {


        EmailverifyDTO Emailverify = EmailverifyDTO
                .builder()
                .accntId(accId)
                .verifyYn("y")
                .build();

        AccountBaseDTO accountBaseDTO = modelMapper.map(Emailverify, AccountBaseDTO.class);



        List<MediaVerifyBaseDTO> media = mediaVerifyRepository.findByAccntIdAndMediaValidString(accId, key)
                .stream().map(e -> modelMapper.map(e, MediaVerifyBaseDTO.class)).collect(Collectors.toList());

        logger.info("============= - {}", media.get(0).toString());
//        logger.info("=============accto - {}", accntEntity.toString());

        for (MediaVerifyBaseDTO m : media) {
            if (m.getMediaValidString().equals(key)) {
                logger.info("if init");

//                accountRepository.AccountVerifyUpdate(accId,"y");
                accountRepository.AccountRepositoryDynamicUpdate(accountBaseDTO);
            }
        }


    }

    void emailLogSave(EmailSendRequest req, String verifyNum) {
        List<String> id = Arrays.asList(req.getReceiver());
        MediaVerify entity = MediaVerify
                .builder()
                .accntId(req.getAccountId())            //사용한 어카운트아이디
                .mediaVerifyReqId(id.get(0))   //검증요청아이디
                .mediaTypeCd("SES") //검증미디어유형
                .mediaValidString(verifyNum)   //검증번호
                .verifyTypeCd("email")       //이메일검증
                .expiredDt(LocalDateTime.now().plusDays(7))
                .regDt(LocalDateTime.now())
                .build();
        mediaVerifyRepository.save(entity);
    }

    public void emailTemplateChoiceService(EmailSendRequest esr) {
        String mailForm = "error";
        String vn = VerifyNumber.verifyNumber();

        if (esr.getUseForm().equals("ResetPassword")) {
            List<String> email = Arrays.asList(esr.getReceiver());
            mailForm = mailTemplateReadService
                    .ResetPasswordTemplateRead("ResetPassword", email.get(0));
        }
        if (esr.getUseForm().equals("UserCertification")) {
            String verifyEmail = esr.getReceiver().toString();
            StringBuffer sb = new StringBuffer();
            sb.append("ses/V1/emailcheck?");
            sb.append(String.format("accId=%s", esr.getAccountId()));
            sb.append(String.format("&key=%s", vn));
            mailForm = mailTemplateReadService
                    .UserCertificationReadTemplateRead("UserCertification", sb.toString());
        }
        esr.setCustomerMessage(mailForm);
        emailLogSave(esr, vn);
        emailSendServiceMethod(esr);
    }

    public void emailSendServiceMethod(EmailSendRequest esr) {
        awsSesService.TextSendMail(
                esr.getReceiver(),
                devemail,
                esr.getCustomerMessage(),
                esr.getUseForm());
        logger.info("seccess");
    }

    public void emailCheckTemplate(EmailSendRequest esr, HttpServletRequest req, String tokenCity) {

    }

}
