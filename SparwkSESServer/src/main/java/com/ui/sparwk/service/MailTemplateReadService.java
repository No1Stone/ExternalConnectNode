package com.ui.sparwk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class MailTemplateReadService {
    private final Logger logger = LoggerFactory.getLogger(MailTemplateReadService.class);

    public String UserCertificationReadTemplateRead(String templateName, String emailText) {
        StringBuffer result = new StringBuffer();
        StringBuffer dirPath = new StringBuffer();
        dirPath.append("/mail_template/");
        dirPath.append(templateName);
        dirPath.append(".html");
        ClassPathResource cpr = new ClassPathResource(dirPath.toString());
        logger.info(" cpr pathurl - {}", cpr.toString());
        logger.info("email text - {}",emailText);
            try {
                Path path = Paths.get(cpr.getURI());
                List<String> content = Files.readAllLines(path);
                content.forEach(System.out::println);
                for (String e : content) {
                    if (e.startsWith("                    <a href=\"http://localhost:8028/\"")) {
                        e = String.format("\"                    <a href=\"http://localhost:8028/%s\" style=background: #2a7bf3; border-radius:3px; padding:7px 12px; color: #fff; border: none; outline: none; font-size: 14px; cursor: pointer;>", emailText);
                        logger.info("================= - {}", e.toString());
                        logger.info("================= - {}", emailText);
                    }
                    result.append(e);
                }
                logger.info("append - {}", result.toString());
            } catch (IOException e) {
                logger.error(" - {}", e.getMessage(), e);
            }
        return result.toString();
    }


    public String ResetPasswordTemplateRead(String templateName, String emailText){
        StringBuffer result = new StringBuffer();
        StringBuffer dirPath = new StringBuffer();
        dirPath.append("/mail_template/");
        dirPath.append(templateName);
        dirPath.append(".html");
        ClassPathResource cpr = new ClassPathResource(dirPath.toString());

        logger.info(" cpr pathurl - {}", cpr.toString());
        logger.info("email text - {}",emailText);
        try {
            Path path = Paths.get(cpr.getURI());
            List<String> content = Files.readAllLines(path);
            content.forEach(System.out::println);
            for (String e : content) {
                if (e.startsWith("                    Resistered Email Address:")) {
                    e = String.format("<a style=\"color:#ed6515; margin-left:5px;\" href=\"\">Email : your %s </a>"
                            , emailText);
                    logger.info("================= - {}", e.toString());
                    logger.info("================= - {}", emailText);
                }
                result.append(e);
            }
            logger.info("append - {}", result.toString());
        } catch (IOException e) {
            logger.error(" - {}", e.getMessage(), e);
        }
        return result.toString();
    }


}
