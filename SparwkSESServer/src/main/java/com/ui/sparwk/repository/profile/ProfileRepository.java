package com.ui.sparwk.repository.profile;

import com.ui.sparwk.entity.profile.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<ProfileEntity, Long> {

    Optional<ProfileEntity> findByProfileId(String profileId);


}
