package com.ui.sparwk.repository.account;

import com.ui.sparwk.entity.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> , AccountRepositoryDynamic {

    Optional<Account> findByAccntEmail(String email);

}
