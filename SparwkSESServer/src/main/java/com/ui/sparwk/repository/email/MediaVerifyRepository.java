package com.ui.sparwk.repository.email;

import com.ui.sparwk.entity.email.MediaVerify;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MediaVerifyRepository extends JpaRepository<MediaVerify, Long> {

    List<MediaVerify> findByAccntIdAndMediaValidString(Long accId, String key);

}
