package com.ui.sparwk.repository.account;

import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.ui.sparwk.dto.account.AccountBaseDTO;
import com.ui.sparwk.entity.account.QAccount;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class AccountRepositoryDynamicImpl implements AccountRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(AccountRepositoryDynamicImpl.class);
    private QAccount qAccount = QAccount.account;

    private AccountRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long AccountRepositoryDynamicUpdate(AccountBaseDTO entity) {

        logger.info("dynamic -{}", entity);

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qAccount);

        if (entity.getAccntTypeCd() == null || entity.getAccntTypeCd().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntTypeCd, entity.getAccntTypeCd());
        }
        if (entity.getAccntEmail() == null || entity.getAccntEmail().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntEmail, entity.getAccntEmail());
        }
        if (entity.getAccntPass() == null || entity.getAccntPass().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntPass, entity.getAccntPass());
        }
        if (entity.getCountryCd() == null || entity.getCountryCd().isEmpty()) {
            logger.info("dynamic cont -{}", entity);
        } else {
            jpaUpdateClause.set(qAccount.countryCd, entity.getCountryCd());
        }
        if (entity.getPhoneNumber() == null || entity.getPhoneNumber().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.phoneNumber, entity.getPhoneNumber());
        }
        if (entity.getAccntLoginActiveYn() == null || entity.getAccntLoginActiveYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.accntLoginActiveYn, entity.getAccntLoginActiveYn());
        }
        if (entity.getLastUseProfileId() != null) {
            jpaUpdateClause.set(qAccount.lastUseProfileId, entity.getLastUseProfileId());
        }
        if (entity.getVerifyYn() == null || entity.getVerifyYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.verifyYn, entity.getVerifyYn());
        }
        if (entity.getUseYn() == null || entity.getUseYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qAccount.useYn, entity.getUseYn());
        }
        jpaUpdateClause.where(qAccount.accntId.eq(entity.getAccntId()));
        if (entity.getAccntRepositoryId() != null) {
            jpaUpdateClause.where(qAccount.accntRepositoryId.eq(entity.getAccntRepositoryId()));
        }

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }

    @Override
    public Long AccountVerifyUpdate(Long acc, String yn) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qAccount);
        jpaUpdateClause.set(qAccount.verifyYn, yn);
        jpaUpdateClause.where(qAccount.accntId.eq(acc));
        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;

    }
}
