package com.ui.sparwk.repository.account;

import com.ui.sparwk.dto.account.AccountBaseDTO;
import org.springframework.transaction.annotation.Transactional;

public interface AccountRepositoryDynamic {
    @Transactional
    Long AccountRepositoryDynamicUpdate(AccountBaseDTO entity);

    @Transactional
    Long AccountVerifyUpdate(Long acc, String yn);

}
