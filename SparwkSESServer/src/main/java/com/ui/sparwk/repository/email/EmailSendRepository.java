package com.ui.sparwk.repository.email;

import com.ui.sparwk.entity.email.EmailSendEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmailSendRepository extends JpaRepository<EmailSendEntity, Long>  {
}
