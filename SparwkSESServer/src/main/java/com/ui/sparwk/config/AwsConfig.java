package com.ui.sparwk.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;

@Configuration
public class AwsConfig {

	@Value("${cloud.aws.credentials.accessKey}")
	private String accessKeyId;
	@Value("${cloud.aws.credentials.secretKey}")
	private String secretAccessKey;
//	@Value("${cloud.aws.credentials.region}")
//	private String region;
	//	@Bean
//	public AmazonS3 getAmazonS3Client() {
//		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
//		return AmazonS3ClientBuilder
//				.standard()
//				.withRegion(Regions.fromName(region))
//				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//				.build();
//	}
	//sdk1	
	@Bean
	public AmazonSimpleEmailService AmazonSESClient() {
		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
		return AmazonSimpleEmailServiceClientBuilder
				.standard()
				//.withCredentials(new ProfileCredentialsProvider())
				.withRegion(Regions.AP_NORTHEAST_2)
				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials)) //
				//.withCredentials(new EnvironmentVariableCredentialsProvider())
				.build();
	}
	@Bean
	public RestTemplate restTemplate() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		HttpClient client = HttpClientBuilder.create()
				.setMaxConnTotal(50)
				.setMaxConnPerRoute(20).build();
		factory.setHttpClient(client);
		factory.setConnectTimeout(10000);
		factory.setReadTimeout(5000);
		return new RestTemplate(factory);
	}
}
