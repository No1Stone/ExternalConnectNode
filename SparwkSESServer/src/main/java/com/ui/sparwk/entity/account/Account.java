package com.ui.sparwk.entity.account;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account")
@DynamicUpdate
public class Account {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "accnt_repository_id", nullable = true)
    private Long accntRepositoryId;

    @Column(name = "accnt_type_cd", nullable = true, length = 9)
    private String accntTypeCd;

    @Column(name = "accnt_email", nullable = true, length = 100, unique = true)
    private String accntEmail;

    @Column(name = "accnt_pass", nullable = true, length = 200)
    private String accntPass;

    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;

    @Column(name = "phone_number", nullable = true, length = 20)
    private String phoneNumber;

    @Column(name = "accnt_login_active_yn", nullable = true, length = 1)
    private String accntLoginActiveYn;

    @Column(name = "last_use_profileId")
    private Long lastUseProfileId;

    @Column(name = "verify_yn", nullable = true, length = 1)
    private String verifyYn;

    @Column(name = "use_yn", nullable = true, length = 1)
    private String useYn;

    @Builder
    Account(
            Long accntId,
            Long accntRepositoryId,
            String accntTypeCd,
            String accntEmail,
            String accntPass,
            String countryCd,
            String phoneNumber,
            String accntLoginActiveYn,
            String verifyYn,
            String useYn
    ) {
        this.accntId = accntId;
        this.accntRepositoryId = accntRepositoryId;
        this.accntTypeCd = accntTypeCd;
        this.accntEmail = accntEmail;
        this.accntPass = accntPass;
        this.countryCd = countryCd;
        this.phoneNumber = phoneNumber;
        this.accntLoginActiveYn = accntLoginActiveYn;
        this.verifyYn = verifyYn;
        this.useYn = useYn;
    }

}
