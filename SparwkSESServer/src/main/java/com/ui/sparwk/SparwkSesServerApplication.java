package com.ui.sparwk;

import com.ui.sparwk.entity.profile.ProfileEntity;
import com.ui.sparwk.repository.email.EmailSendRepository;
import com.ui.sparwk.repository.profile.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparwkSesServerApplication {

    @Autowired
    private EmailSendRepository emailSendRepository;
    @Autowired
    private ProfileRepository profileRepository;
    public static void main(String[] args) {
        SpringApplication.run(SparwkSesServerApplication.class, args);
    }



}
