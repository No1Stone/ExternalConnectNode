package com.ui.sparwk.dto.email;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class MediaVerifyBaseDTO {

    private Long accntId;
    private String mediaVerifyReqId;
    private String mediaTypeCd;
    private String mediaValidString;
    private String verifyTypeCd;
    private LocalDateTime expiredDt;
    private LocalDateTime regDt;

}
