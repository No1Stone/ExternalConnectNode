package com.ui.sparwk.dto.account;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmailverifyDTO {

    private Long accntId;
    private String verifyYn;

}
