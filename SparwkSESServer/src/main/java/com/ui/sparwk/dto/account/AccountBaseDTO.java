package com.ui.sparwk.dto.account;

import lombok.*;

import javax.validation.constraints.Size;

@Getter@Setter
@Builder
@NoArgsConstructor@AllArgsConstructor
@ToString
public class AccountBaseDTO {

    private Long accntId;

    private Long accntRepositoryId;

    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String accntTypeCd;

    @Size(max = 100, message = "Account email NotNull")
    private String accntEmail;

    @Size(max = 200, message = "encoding")
    private String accntPass;

    @Size(max = 9, message = "AccounttyCd max lenth = 9")
    private String countryCd;

    @Size(max = 20, message = "")
    private String phoneNumber;

    @Size(max = 1, message = "y or n")
    private String accntLoginActiveYn;

    private Long lastUseProfileId;

    @Size(max = 1, message = "y or n")
    private String verifyYn;

    @Size(max = 1, message = "y or n")
    private String useYn;

}
