package com.ui.sparwk.dto.email;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder@ToString
@AllArgsConstructor@NoArgsConstructor
public class EmailSendRequest {
    private Long accountId;
    private String useForm;
    private String[] receiver;
    private String customerMessage;
}
