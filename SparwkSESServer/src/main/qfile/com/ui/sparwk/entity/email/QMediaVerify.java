package com.ui.sparwk.entity.email;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMediaVerify is a Querydsl query type for MediaVerify
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMediaVerify extends EntityPathBase<MediaVerify> {

    private static final long serialVersionUID = -1253361985L;

    public static final QMediaVerify mediaVerify = new QMediaVerify("mediaVerify");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> expiredDt = createDateTime("expiredDt", java.time.LocalDateTime.class);

    public final StringPath mediaTypeCd = createString("mediaTypeCd");

    public final StringPath mediaValidString = createString("mediaValidString");

    public final StringPath mediaVerifyReqId = createString("mediaVerifyReqId");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final StringPath verifyTypeCd = createString("verifyTypeCd");

    public QMediaVerify(String variable) {
        super(MediaVerify.class, forVariable(variable));
    }

    public QMediaVerify(Path<? extends MediaVerify> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMediaVerify(PathMetadata metadata) {
        super(MediaVerify.class, metadata);
    }

}

