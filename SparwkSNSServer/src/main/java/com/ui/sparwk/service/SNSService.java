package com.ui.sparwk.service;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.AmazonSNSException;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class SNSService {

    private final Logger logger = LoggerFactory.getLogger(SNSService.class);

    @Autowired
    private AmazonSNSClient amazonSNSClient;
    @Autowired
    private AmazonSNS amazonSNS;

    public void test1(String message, String phoneNumber) {

        Map<String, MessageAttributeValue> smsAttributes = new HashMap<>();
        smsAttributes
                .put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                        .withStringValue("mySenderId").withDataType("String"));

        smsAttributes
                .put("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue()
                        .withStringValue("0.50").withDataType("String"));

        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                .withStringValue("Promotional").withDataType("String"));


        sendSMSMessage( message, phoneNumber, smsAttributes);
    }

    public void sendSMSMessage(String message, String phoneNumber, Map<String, MessageAttributeValue> smsAttributes) {


        PublishResult result =
                amazonSNS.publish(new PublishRequest()
                        .withMessage(message)
                        .withPhoneNumber(phoneNumber)
                        .withMessageAttributes(smsAttributes));
        System.out.println(result); // result ex) {MessageId: 7ace45e4-31wr-5353-b2f2-17759c45d913} }

    }
}
