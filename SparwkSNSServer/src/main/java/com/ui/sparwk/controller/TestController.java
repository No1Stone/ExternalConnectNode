package com.ui.sparwk.controller;

import com.ui.sparwk.controller.dto.TestRequest;
import com.ui.sparwk.service.SNSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/V1")
@CrossOrigin("*")
public class TestController {

    @Autowired
    private SNSService snsService;
    private final Logger logger = LoggerFactory.getLogger(TestController.class);
    @PostMapping(path = "/test1")
    public void test1(@RequestBody TestRequest request){
        logger.info(" - {}",request);
        snsService.test1(request.getMessage(), request.getPhoneNumber());
    }

}
