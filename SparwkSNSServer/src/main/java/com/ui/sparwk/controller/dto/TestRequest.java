package com.ui.sparwk.controller.dto;

import com.amazonaws.services.sns.model.MessageAttributeValue;
import lombok.Data;

import java.util.Map;

@Data
public class TestRequest {

    private String message;
    private String phoneNumber;
//    private Map<String, MessageAttributeValue> smsAttributes;
}
