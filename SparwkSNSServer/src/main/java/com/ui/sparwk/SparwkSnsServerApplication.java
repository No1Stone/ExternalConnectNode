package com.ui.sparwk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparwkSnsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SparwkSnsServerApplication.class, args);
    }

}
