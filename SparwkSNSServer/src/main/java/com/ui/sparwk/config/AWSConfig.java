package com.ui.sparwk.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfig {
	
	@Value("${cloud.aws.credentials.accessKey}")
	private String accessKeyId;
	
	@Value("${cloud.aws.credentials.secretKey}")
	private String secretAccessKey;
	
	@Value("${cloud.aws.credentials.region}")
	private String region;


//	@Bean
//	public AmazonSNSClient getAmazonSNSClient(){
//		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
//		return (AmazonSNSClient) AmazonSNSClientBuilder
//				.standard()
//				.withRegion(Regions.AP_NORTHEAST_1)
//				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//				.build();
//	}
	@Bean
	public AmazonSNSClient getAmazonSNSClient(){
		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
		return (AmazonSNSClient) AmazonSNSClientBuilder
				.standard()
				.withRegion(Regions.AP_NORTHEAST_1)
				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
				.build();
	}
//	@Bean
//	public AmazonS3 getAmazonS3Client() {
//		final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
//		return AmazonS3ClientBuilder
//				.standard()
//				.withRegion(Regions.fromName(region))
//				.withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
//				.build();
//	}
//
	//@Bean AmazonSimpleEmailServiceClients
	
}
